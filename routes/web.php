<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('homepage');
Route::get('/admin', 'HomeController@admin')->name('admin');
Route::get('/admin/teacher', 'HomeController@showteach')->name('teacher');
Route::get('/admin/schedule', 'HomeController@showsched')->name('schedule');
Route::get('/admin/subject', 'HomeController@showsubj')->name('subject');
Route::get('/admin/strand', 'HomeController@showstrand')->name('strand');
Route::get('/admin/student', 'HomeController@showstud')->name('showstudent');
Route::get('/admin/settings', 'HomeController@showsettings')->name('settings');
Route::get('/admin/endsem', 'HomeController@endSemester');


// Route::get('/teacher', 'HomeController@showteach')->name('teacher');
Route::post('/adddata', 'AddData@addData')->name('addData');
Route::post('/addTeacher', 'AddData@addTeacher')->name('addTeacher');
Route::post('/addSubject', 'AddData@addSubject')->name('addSubject');
Route::post('/addSched', 'AddData@addSched')->name('addSched');
Route::post('/addStudent', 'AddData@addStudent')->name('addStudent');
Route::post('/addStrand', 'AddData@addStrand')->name('addStrand');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
//Route::resource('tasks', 'TasksController');
Route::post('/editTeacher', 'AddData@editTeacher')->name('editTeacher');
Route::post('/editSubject', 'AddData@editSubject')->name('editSubject');
Route::post('/editSched', 'AddData@editSched')->name('editSched');
Route::post('/editStudent', 'AddData@editStudent')->name('editStudent');
Route::post('/editStrand', 'AddData@editStrand')->name('editStrand');
Route::post('/editSettings', 'AddData@editSettings')->name('editSettings');

Route::post('/deleteTeacher', 'AddData@deleteTeacher')->name('deleteTeacher');
Route::post('/deleteSubject', 'AddData@deleteSubject')->name('deleteSubject');
Route::post('/deleteSched', 'AddData@deleteSched')->name('deleteSched');
Route::post('/deleteStudent', 'AddData@deleteStudent')->name('deleteStudent');
Route::post('/deleteStrand', 'AddData@deleteStrand')->name('deleteStrand');


Route::post('Admin/calendar/update', 'CalendarController@update');
Route::resource('Admin/calendar', 'CalendarController');

Route::prefix('teacher')->group(function() {
    Route::get('/', 'TeacherController@index');
    Route::get('/activity/view/{act_id}/{sched_id}', 'TeacherController@viewAct')->name('viewAct');
    Route::get('/strand/{strand_id}/{sched_id}', 'TeacherController@strand')->name('teachstrand');
    Route::get('/grading/{strand_id}/{sched_id}', 'TeacherController@grading')->name('grading');
    Route::post('/exam/questions/store', 'TeacherController@storeQuestion');
    Route::post('/exam/questions/delete', 'TeacherController@deleteQuestion');
    Route::post('/exam/questions/edit/store', 'TeacherController@storeEdittedQuestion');
    Route::post('/exam/questions/edit', 'TeacherController@editQuestion');
    Route::get('/exam/questions/{strand_id}/{sched_id}/{exam_id}', 'TeacherController@examQuestion');
    Route::get('/exam/questions/{strand_id}/{sched_id}/{exam_id}/results', 'TeacherController@examResults');
    Route::get('/exam/{strand_id}/{sched_id}', 'TeacherController@exam');
    Route::post('/exam/store/{sched_id}', 'TeacherController@examStore');
    Route::post('/exam/getExam', 'TeacherController@getExam');
    Route::patch('/exam/edit', 'TeacherController@editExam');
    Route::post('/exam/delete/{exam_id}', 'TeacherController@deleteExam');
    Route::post('/grading/encode', 'TeacherController@encode');
    Route::post('/grading/getStudent', 'TeacherController@getStudent');
    Route::get('/activity/{strand_id}/{sched_id}', 'TeacherController@activity');
    Route::post('/activity/store/{sched_id}', 'TeacherController@activityStore');
    Route::patch('/activity/edit/{sched_id}', 'TeacherController@activityEdit');
    Route::post('/activity/getAct', 'TeacherController@getAct');
    Route::post('/activity/delete/{id}/{sched_id}', 'TeacherController@activityDelete');
    Route::get('/calendar', 'TeacherController@calendar')->name('teachcalendar');
    Route::post('/calendar/add', 'TeacherController@calendarAdd')->name('teachcalendarAdd');
    Route::patch('/calendar/edit', 'TeacherController@calendarEdit')->name('teachcalendarEdit');
    Route::post('/calendar/delete', 'TeacherController@calendarDelete');
});


Route::prefix('student')->group(function() {
    Route::get('/', 'StudentController@index');
    Route::get('/myclass', 'StudentController@myclass')->name('myclass');
    Route::post('/exams/temp/answer', 'StudentController@storeTempAnswer');
    Route::get('/exams/{sched_id}/{exam_id}', 'StudentController@viewExam');
    Route::get('/exams/{sched_id}/{exam_id}/answer', 'StudentController@answerExam');
    Route::get('/exams/{sched_id}/{exam_id}/answer/submit', 'StudentController@submitExamAnswer');
    Route::get('/exams/{sched_id}', 'StudentController@exams');
    Route::get('/activity/{sched_id}', 'StudentController@Activity');
    Route::get('/activity/{sched_id}/{act_id}', 'StudentController@showActivity');
    Route::post('/activity/answer', 'StudentController@answerActivity');
    Route::get('/grades', 'StudentController@grades')->name('studgrades');
    Route::get('/calendar', 'StudentController@calendar')->name('studcalendar');
    Route::get('/enroll', 'StudentController@enroll');
    Route::get('/enroll/now', 'StudentController@enrollStudent');
});
