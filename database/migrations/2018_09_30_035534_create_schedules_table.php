<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('teacher_profile_id')->nullable();
            $table->unsignedInteger('subject_id');
            $table->text('day');
            $table->time('time_start');
            $table->time('time_end');
            
            // $table->unsignedInteger('teacher_sched');
            // $table->unsignedInteger('student_sched');
            // $table->unsignedInteger('subject_sched');
            $table->timestamps();
        });
        // Schema::table('schedules', function($table)
        // {
        //     $table->foreign('student_sched')->references('teacher_id')->on('teacher_profiles')->onDelete('cascade');
        //     $table->foreign('teacher_sched')->references('student_id')->on('students')->onDelete('cascade');
        //     $table->foreign('subject_sched')->references('subject_id')->on('subjects')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
