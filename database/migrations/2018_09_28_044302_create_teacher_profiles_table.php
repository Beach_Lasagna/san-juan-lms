<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_profiles', function (Blueprint $table) {
            $table->increments('teacher_id');
            $table->text('firstname');
            $table->text('middlename')->nullable();
            $table->text('lastname');
            $table->text('position');
            $table->text('address');
            $table->tinyInteger('age');
            $table->text('birthmonth');
            $table->smallInteger('birthday');
            $table->smallInteger('birthyear');
            $table->text('school_graduated');
            $table->smallInteger('year_graduated');
            $table->text('educational_attainment');
            $table->text('subjects');
            $table->text('contact');
            $table->text('email');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
        // Schema::dropIfExists('subjects');
        Schema::dropIfExists('teacher_profiles');
    }
}
