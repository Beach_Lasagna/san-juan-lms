<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolYearToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('settings', function (Blueprint $table) {
        $table->integer('school_year_start');
        $table->integer('school_year_end');
        $table->string('school_year');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('settings', function (Blueprint $table) {
        $table->dropColumn('school_year_start');
        $table->dropColumn('school_year_end');
        $table->dropColumn('school_year');
      });
    }
}
