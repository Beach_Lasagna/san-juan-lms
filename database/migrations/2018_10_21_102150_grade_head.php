<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GradeHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gradingHeads', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('grade');
          $table->integer('semester');
          $table->integer('student_id');
          $table->integer('strand_id');
          $table->string('school_year');
          $table->decimal('gpa', 2, 2)->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gradingHeads');
    }
}
