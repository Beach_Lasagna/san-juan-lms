<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('student_id');
            $table->text('firstname');
            $table->text('middlename')->nullable();
            $table->text('lastname');
            $table->text('gender');
            $table->bigInteger('lrn');
            $table->tinyInteger('grade');
            $table->text('strand');
            $table->timestamps();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
        // Schema::dropIfExists('subjects');
        Schema::dropIfExists('students');
    }
}
