<?php

use Illuminate\Database\Seeder;

class AddDummyEvent extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $data = [
      ['teacher_profile_id'=>'1',
       'subject_id'=>'2',
       'day'=>'Monday',
       'time_start'=>'9:00',
       'time_end'=>'15:00',
       'strand_id'=>'1'],

       ['teacher_profile_id'=>'1',
        'subject_id'=>'3',
        'day'=>'Wednesday',
        'time_start'=>'11:00',
        'time_end'=>'13:00',
        'strand_id'=>'1'],

        ['teacher_profile_id'=>'1',
         'subject_id'=>'4',
         'day'=>'Thursday',
         'time_start'=>'7:00',
         'time_end'=>'10:00',
         'strand_id'=>'1'],

         ['teacher_profile_id'=>'1',
          'subject_id'=>'5',
          'day'=>'Friday',
          'time_start'=>'12:00',
          'time_end'=>'15:00',
          'strand_id'=>'1'],
     ];
    \DB::table('schedules')->insert($data);
  }
}
