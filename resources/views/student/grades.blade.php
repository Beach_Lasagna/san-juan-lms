@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
            <p class="panel-subtitle">{{$student->lrn}}</p>
            <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
          </div>
          <div class="panel-body">
            @foreach ($student->grades as $gradingHead)
              <table class="table table-striped" style="margin-top: -1%; background-color: #A5F2F3;" >
                <tbody>
                  <tr>
                    <th style="text-align: right; width: 15%;">SCHOOL YEAR</th>
                    <td colspan="4">{{$gradingHead->school_year}}</td>
                    <th style="text-align: right;">Semester</th>
                    <td colspan="2">
                      @if ($gradingHead->semester == 1)
                        First Semester
                      @else
                        Second Semester
                      @endif
                    </td>

                  </tr>
                  <tr>
                    <th style="text-align: right;">Course Code</th>
                    <td colspan="4">{{$gradingHead->strand->strand}}</td>
                    <th style="text-align: right;">Course Description</th>
                    <td colspan="2">{{$gradingHead->strand->strand}}</td>

                  </tr>
                  <tr>
                    <th style="text-align: right;">GPA</th>
                    <td colspan="4">
                      @if ($gradingHead->gpa != null)
                        {{$gradingHead->gpa}}
                      @else
                        0.00
                      @endif
                    </td>
                    <th style="text-align: right;">Adviser</th>
                    <td colspan="2">AEZELL V. WELBA</td>
                  </tr>
                </tbody>
              </table>
              <hr style=" display: block; margin-top: 0.5em;margin-bottom: 0.5em; margin-left: auto; margin-right: auto;border-style: inset;border-width: 1px;">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Subject</th>
                    <th>Teacher</th>
                    <th>Final Grade</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($gradingHead->grades) > 0)
                    @foreach ($gradingHead->grades as $grade)
                      <tr>
                        <td>{{$grade->subject->subject_name}}</td>
                        <td>{{$grade->teacher->lastname}}, {{$grade->teacher->firstname}} {{$grade->teacher->middlename}}</td>
                        <td>{{$grade->grade}}</td>
                        @if($grade->remarks == "Passed")
                          <td class="alert alert-success">
                          @else
                            <td class="alert alert-danger">
                            @endif
                            {{$grade->remarks}}
                          </td>
                        </tr>
                      @endforeach
                    @else
                      @foreach ($strand->schedules->where('grade', $gradingHead->grade)->where('semester', $gradingHead->semester) as $s)
                        <tr>
                          <td>{{$s->subject->subject_name}}</td>
                          @if(isset($s->teacher_profile_id))
                            <td>{{$s->myTeacher->lastname}}, {{$s->myTeacher->firstname}} {{$s->myTeacher->middlename}}</td>
                          @else
                            <td></td>
                          @endif
                          <td></td>
                          <td></td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
                <br><br>
              @endforeach
            </div>
          </div>
        </div>
        <!-- END OVERVIEW -->
      </div>
    </div>

  @endsection
