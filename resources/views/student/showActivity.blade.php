@extends('layouts.app')

@section('content')
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<!-- RECENT PURCHASES -->
						<div class="panel panel-headline">
							<!-- TODO LIST -->
							<div class="panel-heading">
								<h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
								<p class="panel-subtitle">{{$student->lrn}}</p>
								<p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
							</div>
							<div class="panel-body">
								<a href="/student/activity/{{$sched->id}}" class="btn btn-primary">Go Back</a>
								<br><br>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>{{$sched->subject->subject_name}} : {{$activity->title}}</h4>
									</div>
									<div class="panel-body">
										<p>{{$activity->description}}</p>
										@if ($activity->file != "")
											<p>Attached File : <a href="{{asset('storage/assignments/' . $activity->file)}}" download>{{$activity->file}}</a></p>
										@endif
										<hr>
										<small>Published on {{date('M j Y g:i A' ,strtotime($activity->created_at))}} and to be passed on or before {{date('M j Y g:i A' ,strtotime($activity->deadline))}}</small>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>Answer</h4>
									</div>
									<div class="panel-body">
										@if(strtotime(date("r")) < strtotime($activity->deadline))
											@if (count($student->answers->where('activity_id', $activity->id)) > 0)
												<h4>You Answered : <h4><br>
												@foreach ($student->answers->where('activity_id', $activity->id) as $answer)
													<p>{{$answer->answer}}</p>
													@if ($answer->file != "")
														<hr>
														<p>You attached the file <a href="{{asset('storage/submitted-assignments/' . $answer->file)}}">{{$answer->file}}</a></p>
													@endif
												@endforeach
											@else
												<div class="panel-body">
													<form action="{{url('/student/activity/answer')}}" method="post" autocomplete="off" enctype="multipart/form-data">
														@csrf
														<input type="hidden" name="sched_id" value="{{$sched->id}}">
														<input type="hidden" name="act_id" value="{{$activity->id}}">
														<div class="form-group">
															<textarea style="resize: none; width: 100%; height: 200px;" name="answer"></textarea> <br><br>
														</div>
														<div class="form-group">
															<input type="file" name="file">
														</div>
														<hr>
														<button type="submit" class="btn btn-primary pull-right">Submit</button>
													</form>
												@endif
											@else
												<p>You cannot answer this assignment anymore.</p>
											@endif
										</div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->

		@endsection
