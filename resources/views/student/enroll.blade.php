@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="col-md-12" >
        <div class="panel panel-headline">
          <div class="panel-heading">
            <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
            <p class="panel-subtitle">{{$student->lrn}}</p>
            <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
          </div>
          <div class="panel-body">
            @if (session()->has('success'))
              <div class="alert alert-success">
                <p>{{session('success')}}</p>
              </div>
            @endif
            @if ($setting->enrollment)
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Subject</th>
                    <th>Teacher</th>
                    <th>Time</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($strand->schedules->where('semester', $setting->semester)->where('grade', $student->grade) as $sched)
                    <tr>
                      <td>{{$sched->subject->subject_name}}</td>
                      <td>{{$sched->myTeacher->lastname}}, {{$sched->myTeacher->firstname}} {{$sched->myTeacher->middlename}}</td>
                      <td>{{$sched->time_start}} - {{$sched->time_start}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            @else
              <div class="alert alert-danger">
                <p>Enrollment is currently closed</p>
              </div>
            @endif
            <br>
            @if(!$student->enrolled)
              <a class="btn btn-primary pull-right" href="/student/enroll/now">Enroll Now!</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@endsection
