@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- RECENT PURCHASES -->
            <div class="panel panel-headline">
              <!-- TODO LIST -->
              <div class="panel-heading">
                <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
                <p class="panel-subtitle">{{$student->lrn}}</p>
                <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
              </div>
              <div class="panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4>{{$sched->subject->subject_name}} : {{$exam->title}}</h4>
                  </div>
                  <div class="panel-body">
                    <form id='exam'>
                      <table>
                        @php $num = 0 @endphp
                        @foreach ($exam->questions as $question)
                          <tr>
                            <td style="padding-right: 10px;"><h4>{{++$num}}</h4></td>
                            <td><h4>{{$question->question}}</h4></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><label><input type="radio" id="answer-{{$question->id}}" name="answer-{{$question->id}}" value="A" required>{{$question->A}}</label></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><label><input type="radio" id="answer-{{$question->id}}" name="answer-{{$question->id}}" value="B">{{$question->B}}</label></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><label><input type="radio" id="answer-{{$question->id}}" name="answer-{{$question->id}}" value="C">{{$question->C}}</label></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><label><input type="radio" id="answer-{{$question->id}}" name="answer-{{$question->id}}" value="D">{{$question->D}}</label></td>
                          </tr>
                        @endforeach
                      </table>
                    </div>
                  </div>
                  <hr>
                  <button class="btn btn-primary pull-right">Submit</button>
                </form>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->

  <!-- modal -->
  <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Confirm Submit</h3>
        </div>
        <div class="modal-body">
          <div class="alert alert-warning">
            <p>Are you sure you want to submit?</p>
            <p>Once submitted you won't be able to change your answers</p>
          </div>
          <div class="modal-footer">
            <div class="row">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <a class="btn btn-primary" href="/student/exams/{{$sched->id}}/{{$exam->id}}/answer/submit">Submit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function() {
    $('#confirm').click(function () {
      $('#confirmModal').modal('show');
    });

    $("[id^=answer-]").change(function(e) {
      var question_id = e.target.id.substring(e.target.id.indexOf("-") + 1);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/student/exams/temp/answer',
        type: 'POST',
        data: {
          student: {{$student->student_id}},
          question: question_id,
          answer: $(this).val()
        },
        success:function() {}
      });
    });

    $('#exam').submit(function(e) {
      e.preventDefault();
      $('#confirmModal').modal('show');
    })
  });
  </script>
@endsection
