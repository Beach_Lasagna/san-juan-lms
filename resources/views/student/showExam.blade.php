@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- RECENT PURCHASES -->
            <div class="panel panel-headline">
              <!-- TODO LIST -->
              <div class="panel-heading">
                <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
                <p class="panel-subtitle">{{$student->lrn}}</p>
                <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
              </div>
              <div class="panel-body">
                <a href="/student/exams/{{$sched->id}}" class="btn btn-primary">Go Back</a>
                <br><br>
                @if(session()->has('ExamDone'))
                  <div class="alert alert-success">
                    <p>{{session('ExamDone')}}</p>
                  </div>
                  <br>
                @endif
                @if(session()->has('alreadyAnswered'))
                  <div class="alert alert-danger">
                    <p>{{session('alreadyAnswered')}}</p>
                  </div>
                  <br>
                @endif
                @if(isset($examHead) > 0)
                  @if($examHead->submitted == true)
                    <div @if($examHead->remarks == "Passed") class="alert alert-success" @else class="alert alert-danger" @endif>
                      <h4>You {{$examHead->remarks}} this Exam with the score of {{$examHead->score}} out of {{count($examHead->details)}}</h4>
                    </div>
                  @endif
                @endif
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4>{{$sched->subject->subject_name}} : {{$exam->title}}</h4>
                  </div>
                  <div class="panel-body">
                    <p>{{$exam->description}}</p>
                    <hr>
                    @if(strtotime(date("r")) <= strtotime($exam->deadline))
                      <a class="btn btn-success pull-right" href="/student/exams/{{$sched->id}}/{{$exam->id}}/answer">Start Answering</a>
                    @endif
                    <small>Published on {{date('M j Y g:i A' ,strtotime($exam->created_at))}} and to be answered on or before {{date('M j Y g:i A' ,strtotime($exam->deadline))}}</small>
                  </div>
                </div>
              </div>
              <!-- END RECENT PURCHASES -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->

  <!-- modal -->
  <div class="modal fade" id="startActivityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add Activity</h3>
        </div>
        <div class="modal-body">
          <div class="modal-footer">
            <div class="row">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <a class="btn btn-primary">Submit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function() {
    $('#startActivity').click(function () {
      $('#startActivityModal').modal('show');
    });
  })
  </script>
@endsection
