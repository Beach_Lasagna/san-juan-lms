@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="col-md-12" >
        <div class="panel panel-headline">
          <div class="panel-heading">
            <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
            <p class="panel-subtitle">{{$student->lrn}}</p>
            <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
          </div>
          <div class="panel-body">
            <a href="/student/myclass" class="btn btn-primary">Go Back</a>
            <h3>{{$sched->subject->subject_name}}</h3>
            <table class="table table-striped" align="center">
              <thead>
                <tr>
                  <th>Activity Title</th>
                  <th>Date Published</th>
                  <th>Deadline</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if(count($activities) > 0)
                  @foreach ($activities as $act)
                    <tr>
                      <td>{{$act->title}}</td>
                      <td>{{date('M j Y g:i A' ,strtotime($act->created_at))}}</td>
                      <td>{{date('M j Y g:i A' ,strtotime($act->deadline))}}</td>
                      <td><a href="/student/activity/{{$sched->id}}/{{$act->id}}" class="btn btn-success">View</a></td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td><h3>No Activities Found.</h3></td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@endsection
