@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <!-- TODO LIST -->
        <div class="panel-heading">
          <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
          <p class="panel-subtitle">{{$student->lrn}}</p>
          <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
        </div>
        <div class="panel-body">
          <table class="table table-striped" align="center">
            <thead>
              <tr>
                <th>Subject</th>
                <th>Teacher</th>
                <th>Day</th>
                <th>Time</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if($student->enrolled)
                @foreach ($strand->schedules->where('semester', $setting->semester)->where('grade', $student->grade) as $sched)
                  <tr>
                    <td>{{$sched->subject->subject_name}}</td>
                    @if(isset($sched->teacher_profile_id))
                      <td>{{$sched->myTeacher->firstname}} {{$sched->myTeacher->middlename}} {{$sched->myTeacher->lastname}}</td>
                    @else
                      <td></td>
                    @endif
                    <td>{{$sched->day}}</td>
                    <td>{{date('g:i A', strtotime($sched->time_start))}} - {{date('g:i A', strtotime($sched->time_end))}}</td>
                    <td>
                      ({{count($sched->myActivities->where('deadline', '>=', date("Y-m-d G:i:s", strtotime(date("r")))))}} on going activity,
                      {{count($sched->exams->where('deadline', '>=', date("Y-m-d G:i:s", strtotime(date("r")))))}} on going Exam)
                    </td>
                    @if(isset($sched->teacher_profile_id))
                      <td><a class="btn btn-primary" href="activity/{{$sched->id}}">Activities</a></td>
                      <td><a class="btn btn-primary" href="exams/{{$sched->id}}">Exams</a></td>
                    @else
                      <td></td>
                      <td></td>
                    @endif
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- END TODO LIST -->
      </div>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
