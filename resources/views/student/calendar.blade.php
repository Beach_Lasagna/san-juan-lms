@extends('layouts.app')

@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
          <div class="panel-header text-center"><h3>Calendar</h3></div>
          <div id="calendar" class="panel-body" style="background-color: white; "></div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
  <script>

  $(document).ready(function() {
    showCalendar();
  });

  function showCalendar() {
    var calendar = $('#calendar').fullCalendar({
      header:{
        right:'next',
        center:'title',
        left:'prev'
      },
      events: {!! $events !!},
    });
  }

</script>
@endsection
