@extends('layouts.app')

@section('content')
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <h3 class="panel-title">{{$student->firstname}} {{$student->middlename}} {{$student->lastname}}</h3>
            <p class="panel-subtitle">{{$student->lrn}}</p>
            <p class="panel-subtitle">Grade {{$student->grade}} - {{$strand->strand}}</p>
          </div>
          <div class="panel-body">
            <table class='table table-bordered text-center'>
              <tr>
                <th></th>
                <th class="text-center">Monday</th>
                <th class="text-center">Tuesday</th>
                <th class="text-center">Wednesday</th>
                <th class="text-center">Thursday</th>
                <th class="text-center">Friday</th>

                <tr id='tr-600am'>
                  <td rowspan='2'>6:00 AM</td>
                </tr>
                <tr id='tr-630am'>
                </tr>

                <tr id='tr-700am'>
                  <td rowspan='2'>7:00 AM</td>
                </tr>
                <tr id='tr-730am'>
                </tr>

                <tr id='tr-800am'>
                  <td rowspan='2'>8:00 AM</td>
                </tr>
                <tr id='tr-830am'>
                </tr>

                <tr id='tr-900am'>
                  <td rowspan='2'>9:00 AM</td>
                </tr>
                <tr id='tr-930am'>
                </tr>

                <tr id='tr-1000am'>
                  <td rowspan='2'>10:00 AM</td>
                </tr>
                <tr id='tr-1030am'>
                </tr>

                <tr id='tr-1100am'>
                  <td rowspan='2'>11:00 AM</td>
                </tr>
                <tr id='tr-1130am'>
                </tr>

                <tr id='tr-1200pm'>
                  <td rowspan='2'>12:00 PM</td>
                </tr>
                <tr id='tr-1230pm'>
                </tr>

                <tr id='tr-100pm'>
                  <td rowspan='2'>1:00 PM</td>
                </tr>
                <tr id='tr-130pm'>
                </tr>

                <tr id='tr-200pm'>
                  <td rowspan='2'>2:00 PM</td>
                </tr>
                <tr id='tr-230pm'>
                </tr>

                <tr id='tr-300pm'>
                  <td rowspan='2'>3:00 PM</td>
                </tr>
                <tr id='tr-330pm'>
                </tr>

                <tr id='tr-400pm'>
                  <td rowspan='2'>4:00 PM</td>
                </tr>
                <tr id='tr-430pm'>
                </tr>

                <tr id='tr-500pm'>
                  <td rowspan='2'>5:00 PM</td>
                </tr>
                <tr id='tr-530pm'>
                </tr>

                <tr id='tr-600pm'>
                  <td rowspan='2'>6:00 PM</td>
                </tr>
                <tr id='tr-630pm'>
                </tr>

                <tr id='tr-700pm'>
                  <td rowspan='2'>7:00 PM</td>
                </tr>
                <tr id='tr-730pm'>
                </tr>

                <tr id='tr-800pm'>
                  <td rowspan='2'>8:00 PM</td>
                </tr>
                <tr id='tr-830pm'>
                </tr>

                <tr id='tr-900pm'>
                  <td rowspan='2'>9:00 PM</td>
                </tr>
                <tr id='tr-930pm'>
                </tr>
              </table>

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- END MAIN -->

@endsection


@section('js')
  <script type="text/javascript">
  var last_time = '600am';
  var time_start;

  $(document).ready(function() {

    @if (count($strand->schedules()->where('day','Monday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get()) > 0 && $student->enrolled)
    @foreach ($strand->schedules()->where('day','Monday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get() as $mon)
    time_start = "{!! date('gia',strtotime($mon->time_start)) !!}";
    addTD(time_start);
    $("#tr-{!! date('gia',strtotime($mon->time_start)) !!}").append("<td rowspan='{!! (date('G',strtotime($mon->time_end)) - date('G',strtotime($mon->time_start)))*2 !!}' class='success align-middle'>{!! $mon->subject->subject_name !!}<br>Grade {!! $mon->strand->grade !!} - {!! $mon->strand->strand !!}</td>");
    last_time = "{!! date('gia',strtotime($mon->time_end)) !!}";
    @endforeach
    @endif
    addTD("1000pm");
    last_time = '600am';

    @if (count($strand->schedules()->where('day','Tuesday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get()) > 0 && $student->enrolled)
    @foreach ($strand->schedules()->where('day','Tuesday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get() as $tues)
    time_start = "{!! date('gia',strtotime($tues->time_start)) !!}";
    addTD(time_start);
    $("#tr-{!! date('gia',strtotime($tues->time_start)) !!}").append("<td rowspan='{!! (date('G',strtotime($tues->time_end)) - date('G',strtotime($tues->time_start)))*2 !!}' class='success align-middle'>{!! $tues->subject->subject_name !!}<br>Grade {!! $tues->strand->grade !!} - {!! $tues->strand->strand !!}</td>");
    last_time = "{!! date('gia',strtotime($tues->time_end)) !!}";
    @endforeach
    @endif
    addTD("1000pm");
    last_time = '600am';

    @if (count($strand->schedules()->where('day','Wednesday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get()) > 0 && $student->enrolled)
    @foreach ($strand->schedules()->where('day','Wednesday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get() as $wed)
    time_start = "{!! date('gia',strtotime($wed->time_start)) !!}";
    addTD(time_start);
    $("#tr-{!! date('gia',strtotime($wed->time_start)) !!}").append("<td rowspan='{!! (date('G',strtotime($wed->time_end)) - date('G',strtotime($wed->time_start)))*2 !!}' class='success align-middle'>{!! $wed->subject->subject_name !!}<br>Grade {!! $wed->strand->grade !!} - {!! $wed->strand->strand !!}</td>");
    last_time = "{!! date('gia',strtotime($wed->time_end)) !!}";
    @endforeach
    @endif
    addTD("1000pm");
    last_time = '600am';

    @if (count($strand->schedules()->where('day','Thursday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get()) > 0 && $student->enrolled)
    @foreach ($strand->schedules()->where('day','Thursday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get() as $thurs)
    time_start = "{!! date('gia',strtotime($thurs->time_start)) !!}";
    addTD(time_start);
    $("#tr-{!! date('gia',strtotime($thurs->time_start)) !!}").append("<td rowspan='{!! (date('G',strtotime($thurs->time_end)) - date('G',strtotime($thurs->time_start)))*2 !!}' class='success align-middle'>{!! $thurs->subject->subject_name !!}<br>Grade {!! $thurs->strand->grade !!} - {!! $thurs->strand->strand !!}</td>");
    last_time = "{!! date('gia',strtotime($thurs->time_end)) !!}";
    @endforeach
    @endif
    addTD("1000pm");
    last_time = '600am';

    @if (count($strand->schedules()->where('day','Friday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get()) > 0 && $student->enrolled)
    @foreach ($strand->schedules()->where('day','Friday')->where('semester', $setting->semester)->where('grade', $student->grade)->orderBy('time_start', 'des')->get() as $fri)
    time_start = "{!! date('gia',strtotime($fri->time_start)) !!}";
    addTD(time_start);
    $("#tr-{!! date('gia',strtotime($fri->time_start)) !!}").append("<td rowspan='{!! (date('G',strtotime($fri->time_end)) - date('G',strtotime($fri->time_start)))*2 !!}' class='success align-middle'>{!! $fri->subject->subject_name !!}<br>Grade {!! $fri->strand->grade !!} - {!! $fri->strand->strand !!}</td>");
    last_time = "{!! date('gia',strtotime($fri->time_end)) !!}";
    @endforeach
    @endif
    addTD("1000pm");
    last_time = '600am';

  });

  function addTD(time_start) {
    while(last_time != time_start){
      $("#tr-" + last_time).append("<td></td>");
      if(last_time == "1000am") {
        last_time = "1030am";
      } else if(last_time == "1130am") {
        last_time = "1200pm";
      } else if(last_time == "1230pm") {
        last_time = "100pm";
      } else if(last_time.indexOf("00") > 0) {
        last_time = last_time.replace("00", "30");
      } else {
        if(last_time.length == 5) {
          var hour = parseInt(last_time.substring(0, 1)) + 1;
          var md = last_time.substring(3);
          last_time = hour + "00" + md;
        } else {
          var hour = parseInt(last_time.substring(0, 2)) + 1;
          var md = last_time.substring(4);
          last_time = hour + "00" + md;
        }
      }
    }
  }
  </script>
@endsection
