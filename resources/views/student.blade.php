@extends('layouts.app')


@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
          <div  class="panel-body">
            <h4>Students</h4>
            <table id="tablestudent" class="table table-striped">
              <thead>
                <tr>
                  <th>Lastname</th>
                  <th>Firstname</th>
                  <th>Middlename</th>
                  <th>Gender</th>
                  <th>LRN</th>
                  <th>Grade</th>
                  <th>Strand</th>
                  <th>Form 137</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($stud as $students)
                  <tr>
                    <td>{{ $students->lastname }}</td>
                    <td>{{ $students->firstname }}</td>
                    <td>{{ $students->middlename }}</td>
                    <td>{{ $students->gender }}</td>
                    <td>{{ $students->lrn }}</td>
                    <td>{{ $students->grade }}</td>
                    <td>{{$students->myStrand->strand}}</td>
                    <td><a href="//localhost:8080/birt-viewer/frameset?__report=F137FRONT.rptdesign&id={{$students->id}}&__format=PDF" class="btn btn-warning">Print</a></td>
                    <td> <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#edit{{ $students->student_id }}"><span class="glyphicon glyphicon-pencil"></span></button>
                      <!-- Edit Modal-->

                      <div id="edit{{ $students->student_id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- edit Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit Student</h4>
                            </div>
                            <div class="modal-body">
                              <form method="POST" action="#" id="editstudentform">
                                @csrf
                                <input type="hidden" name="id" value="{{ $students->student_id }}">
                                <div class="form-group">
                                  <label for="firstname">Firstname : </label>
                                  <input type="text" name="firstname" value="{{ $students->firstname }}" class="form-control">
                                  @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                  @endif
                                </div>
                                <div class="form-group">
                                  <label for="middlename">Middlename : </label>
                                  <input type="text" name="middlename" value="{{ $students->middlename }}" class="form-control">
                                  @if ($errors->has('middlename'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('middlename') }}</strong>
                                    </span>
                                  @endif
                                </div>
                                <div class="form-group">
                                  <label for="lastname">Lastname : </label>
                                  <input type="text" name="lastname" value="{{ $students->lastname }}" class="form-control">
                                  @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                  @endif
                                </div>
                                <!-- <div class="form-group">

                                <label for="email">Email</label>
                                <input type="text" name="email" >
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                              </span>
                            @endif
                          </div> -->
                          <div class="form-group">
                            <label for="gender">Gender : </label>
                            @if ($students->gender == "F")
                              {!! '<div class="checkbox-inline"><input type="radio" name="gender" value="M" >Male</div>
                                <div class="checkbox-inline"><input type="radio" name="gender" value="F" checked>Female</div>' !!}
                              @elseif ($students->gender == "M")
                                {!! '<div class="checkbox-inline"><input type="radio" name="gender" value="M" checked >Male</div>
                                  <div class="checkbox-inline"><input type="radio" name="gender" value="F">Female</div>' !!}
                                @endif<br>
                              </div>
                              <div class="form-group">
                                <label for="lrn">LRN : </label>
                                <input type="number" name="lrn" value="{{ $students->lrn }}" class="form-control">
                                @if ($errors->has('lrn'))
                                  <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lrn') }}</strong>
                                  </span>
                                @endif
                              </div>
                              <div class="form-group">
                                <label for="grade">Grade : </label>
                                <select name="grade" value="{{ $students->grade }}" class="form-control">
                                  <option value=11>11</option>
                                  <option value=12>12</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="strand">Strand : </label>
                                <select name="strand" value="{{$students->myStrand->strand}}" class="form-control">
                                  @foreach ($strand as $s)
                                    <option value="{{$s->id}}">{{$s->strand}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit"  class="btn btn-primary editstudent" >Update Info</button>
                              </div>
                            </form>
                          </div></div>
                        </div>
                      </div>

                      <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-id= "{{ $students->student_id }}" data-target="#{{ $students->student_id }}"><span class="glyphicon glyphicon-trash"></span></button>

                      <!-- Delete Modal -->
                      <div id="{{ $students->student_id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Delete Confirmation</h4>
                            </div>
                            <div class="modal-body">
                              <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                              <form id="deletestud" method="POST" action="{{ route('deleteStudent')}}" >
                                @csrf
                                <input type="hidden" name="student_id" value="{{ $students->student_id }}">
                                <input type="hidden" name="user_id" value="{{ $students->user_id }}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger deletebtn ">Delete</button>
                              </form>
                            </div>
                          </div>

                        </div>
                      </div>
                    </td>


                  </tr>
                @endforeach
              </tbody>
            </table>

            <br><br><br>

            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddStud">Add Student</button>
          </div>
          <!-- Modal -->
          <div id="AddStud" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Add Student</h4>
                </div>
                <form method="POST" action="#" id="studentform">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group">
                      <label for="firstname">Firstname : </label>
                      <input class="form-control" type="text" name="firstname">
                      @if ($errors->has('firstname'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="middlename">Middlename : </label>
                      <input type="text" name="middlename" class="form-control">
                      @if ($errors->has('middlename'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('middlename') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="lastname">Lastname : </label>
                      <input type="text" name="lastname" class="form-control">
                      @if ($errors->has('lastname'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">

                      <label for="email">Email : </label>
                      <input type="text" name="email" class="form-control">
                      @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="gender">Gender : </label>
                      <div class="checkbox-inline">
                        <input type="radio" name="gender" value="M">Male
                      </div>
                      <div class="checkbox-inline">
                        <input type="radio" name="gender" value="F">Female
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="lrn">LRN : </label>
                      <input type="number" name="lrn" class="form-control">
                      @if ($errors->has('lrn'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('lrn') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="grade">Grade : </label>
                      <select name="grade" class="form-control">
                        <option value=11>11</option>
                        <option value=12>12</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="strand">Strand : </label>
                      <select name="strand" class="form-control">
                        @foreach ($strand as $s)
                          <option value="{{$s->id}}">{{$s->strand}}</option>
                        @endforeach
                      </select>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitstudent" >Register</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
<!-- Trigger the modal with a button -->

@section('js')
  <script>
  $( ".submitstudent" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "{{ route('addStudent') }}",
      data: $("#studentform").serialize(),
      success: function(store) {
        $( "#tablestudent" ).load( "{{ route('showstudent') }} #tablestudent" );
        $("#studentform").trigger("reset");
      },
      error: function() {
      },
    });
  });
  $( ".editstudent" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "{{ route('editStudent') }}",
      data: $("#editstudentform").serialize(),
      success: function(store) {
        $( "#tablestudent" ).load( "{{ route('showstudent') }} #tablestudent" );
        $("#editstudentform").trigger("reset");
      },
      error: function() {
      },
    });
  });
  // $( ".deletebtn" ).click(function(e) {
  //   e.preventDefault();
  //   $('.modal').modal('hide');
  //   $('#processing').modal('show');
  //   setTimeout(function() {
  //     $('#processing').modal('hide');
  //   }, 987);
  //   $.ajax({
  //     type: "POST",
  //     url: "{{ route('deleteStudent')}}",
  //     data: $("#deletestud").serialize(),
  //     success: function(store) {
  //       $( "#tablestudent" ).load( "{{ route('showstudent') }} #tablestudent" );
        
  //     },
  //     error: function() {
  //     },
  //   });
  // });
</script>
@endsection
