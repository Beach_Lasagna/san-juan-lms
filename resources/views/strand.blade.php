@extends('layouts.app')


@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
          <div class="panel-body">
            <h4>Strands for Grade 11</h4>
            <table id="tablestrand" class="table table-striped">
              <thead>
                <tr>
                  <th>Grade</th>
                  <th>Strand</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($strand->where('grade', 11) as $str)
                  <tr>
                    <td>{{ $str->grade }}</td>
                    <td>{{ $str->strand }}</td>
                    <td>
                      <button type="button" id = "modaledit" class="btn btn-default btn-lg" data-toggle="modal" data-target="#edit{{ $str->id }}" onClick="document.getElementById('selectgrade').value='{{ $str->grade }}'"><span class="glyphicon glyphicon-pencil"></span></button>
                      <!-- Edit Modal -->
                      <div id="edit{{ $str->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit Strand</h4>
                            </div>
                            <form method="POST" action="{{ route('editStrand') }}" id="editstrandform">
                              @csrf
                              <input type="hidden" name="id" value="{{ $str->id }}">
                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="grade">Grade : </label>
                                  <select name="grade" id="selectgrade" class="form-control">
                                    <option value=11 selected >11</option>
                                    <option value=12>12</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="strand">Strand : </label>
                                  <input type="text" name="strand" value="{{ $str->strand }}" class="form-control">
                                  @if ($errors->has('strand'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('strand') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary editstrand" >Edit</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" id="deletesubject" data-target="#{{ $str->id }}"><span class="glyphicon glyphicon-trash"> </button>
                      <!-- Delete Modal -->
                      <div id="{{ $str->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Delete Confirmation</h4>
                            </div>
                            <div class="modal-body">
                              <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                              <form method="POST" action="{{ route('deleteStrand') }}" id="deletestrandform">
                                @csrf
                                <input type="hidden" name="id" value="{{ $str->id }}">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="submit" data-id="{{ $str->id }}" class="btn btn-default deletestrand">Delete</button>
                              </form>

                            </div>
                          </div>

                        </div>
                      </div>

                    </td>
                  </tr>

                @endforeach
              </table><hr>
              <h4>Strands for Grade 12</h4>
              <table  class="table table-striped">
              <thead>
                <tr>
                  <th>Grade</th>
                  <th>Strand</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($strand->where('grade', 12) as $str)
                  <tr>
                    <td>{{ $str->grade }}</td>
                    <td>{{ $str->strand }}</td>
                    <td>
                      <button type="button" id = "modaledit" class="btn btn-default btn-lg" data-toggle="modal" data-target="#edit{{ $str->id }}" onClick="document.getElementById('selectgrade').value='{{ $str->grade }}'"><span class="glyphicon glyphicon-pencil"></span></button>
                      <!-- Edit Modal -->
                      <div id="edit{{ $str->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit Strand</h4>
                            </div>
                            <form method="POST" action="{{ route('editStrand') }}" id="editstrandform">
                              @csrf
                              <input type="hidden" name="id" value="{{ $str->id }}">
                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="grade">Grade </label>
                                  <select name="grade" id="selectgrade" class="form-control">
                                    <option value=11>11</option>
                                    <option value=12 selected>12</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="strand">Strand : </label>
                                  <input type="text" name="strand" value="{{ $str->strand }}" class="form-control">
                                  @if ($errors->has('strand'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('strand') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary editstrand" >Edit</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" id="deletesubject" data-target="#{{ $str->id }}"><span class="glyphicon glyphicon-trash"> </button>
                      <!-- Delete Modal -->
                      <div id="{{ $str->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Delete Confirmation</h4>
                            </div>
                            <div class="modal-body">
                              <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                              <form method="POST" action="{{ route('deleteStrand') }}" id="deletestrandform">
                                @csrf
                                <input type="hidden" name="id" value="{{ $str->id }}">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="submit" data-id="{{ $str->id }}" class="btn btn-default deletestrand">Delete</button>
                              </form>

                            </div>
                          </div>

                        </div>
                      </div>

                    </td>
                  </tr>

                @endforeach
              </table>

              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddStrand">Add Strand</button>
              <!-- Modal -->
              <div id="AddStrand" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Add Strand</h4>
                    </div>
                    <form method="POST" action="#" id="strandform">
                      @csrf
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="grade">Grade : </label>
                          <select name="grade" class="form-control">
                            <option value=11>11</option>
                            <option value=12>12</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="strand">Strand : </label>
                          <input type="text" name="strand" class="form-control">
                          @if ($errors->has('strand'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('strand') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary submitstrand" >Add</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  @endsection

  @section('js')
    <script>
    $( ".submitstrand" ).click(function(e) {
      e.preventDefault();
      $('.modal').modal('hide');
      $('#processing').modal('show')
      setTimeout(function() {
        $('#processing').modal('hide');
      }, 2000);
      $.ajax({
        type: "POST",
        url: "{{ route('addStrand') }}",
        data: $("#strandform").serialize(),
        success: function(store) {
          $( "#tablestrand" ).load( "{{ route('strand') }} #tablestrand" );
          $("#strandform").trigger("reset");
        },
        error: function() {
        },
      });
    });
  </script>
  <script>

  $( ".deletestrand" ).click(function(e) {
    // var id = $(this).data("id");
    // e.preventDefault();
    //   $('.modal').modal('hide');
    //   $('#processing').modal('show')
    //   setTimeout(function() {
    //   $('#processing').modal('hide');
    //    }, 2000);
    //   $.ajax({
    //       type: "DELETE",
    //       url: "{{ route('deleteStrand') }}",
    //       data: 'id': id,
    //       success: function(store) {
    //         $( "#tablestrand" ).load( "{{ route('strand') }} #tablestrand" );
    //       },
    //       error: function() {
    //       },
    //   });
    // });
    </script>
    <script type="text/javascript">

    </script>
    @endsection
