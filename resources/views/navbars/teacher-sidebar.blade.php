<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{{ route('homepage') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <li>
          <a href="#subPages1" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/class-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Classes</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPages1" class="collapse ">
            <ul class="nav">
              @foreach ($scheds as $sched)
                <li><a href="{{ URL::to('teacher/strand/' . $sched->strand_id. '/' .$sched->id ) }}" class="">Grade {{$sched->strand->grade}} : {{$sched->strand->strand}}<br>{{$sched->subject->subject_name}}</a></li>
              @endforeach
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPages4" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/calendar-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Calendar</span><i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPages4" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('teachcalendar') }}" class="">View Calendar</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- END LEFT SIDEBAR -->
