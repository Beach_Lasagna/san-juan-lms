<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{{ route('homepage') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <li><a href="{{ route('myclass') }}" class=""><img src="{{asset('images/icons/subject-icon.png')}}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>My Class</span></a></li>
        <li><a href="{{ route('studgrades') }}" class=""><i class="lnr lnr-file-empty"></i> <span>Grades</span></a></li>
        <li><a href="{{ route('studcalendar') }}" class=""><img src="{{asset('images/icons/calendar-icon.png')}}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Calendar</span></a></li>
        <li><a href="/student/enroll" class=""><img src="{{asset('images/icons/enroll-icon.png')}}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Enrollment</span></a></li>
      </ul>
    </nav>
  </div>
</div>
<!-- END LEFT SIDEBAR -->
