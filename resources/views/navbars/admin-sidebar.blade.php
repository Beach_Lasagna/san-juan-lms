<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{{ route('admin') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <li>
          <a href="#subPagesStudent" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/student-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Students</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesStudent" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('showstudent') }}" class="">View Students List</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPagesFaculty" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons//teacher-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Faculty</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesFaculty" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('teacher') }}" class="">View Teacher List</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPagesSubject" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/subject-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Subjects</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesSubject" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('subject') }}" class="">View Subject List</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPagesStrand" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/class-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Strands</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesStrand" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('strand') }}" class="">View Class Strands</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPagesSched" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/schedule-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Schedules</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesSched" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('schedule') }}" class="">View Class Schedules</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subPagesSetting" data-toggle="collapse" class="collapsed"><img src="{{ asset('images/icons/settings-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" class="invert" > <span>Settings</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
          <div id="subPagesSetting" class="collapse ">
            <ul class="nav">
              <li><a href="{{ route('settings') }}" class="">View Settings</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  </div>
</div>
