

<!--Calendar Modal -->
<div class="modal fade" id="addEventModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Add New Event</h3>
      </div>
      <div class="modal-body">
        <form action="{{url('/Admin/calendar')}}" method="post" autocomplete="off" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="title">Event Title : </label>
            <input name="title" id="title" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="start_date">Start Date : </label>
            <input name="start_date" id="start_date" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="end_date">End Date : </label>
            <input name="end_date" id="end_date" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="description">Event Description : </label>
            <textarea name="description" id="description" class="form-control" style="resize: none;"></textarea>
          </div><br>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <form action="{{action('CalendarController@destroy', 1)}}" method="post" class="pull-right">
          @csrf
          <input name="_method" type="hidden" value="DELETE">
          <input name="delete_id" type="hidden" value="DELETE" id="delete_id">
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
        <h3 class="modal-title" id="exampleModalLongTitle">Edit Event</h3>
      </div>
      <div class="modal-body">
        <form action="{{action('CalendarController@update', 1)}}" method="post" autocomplete="off" enctype="multipart/form-data">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <input id="event_id" name="event_id" type="hidden">
          <div class="form-group">
            <label for="title">Event Title : </label>
            <input name="title" id="title_edit" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="start_date">Start Date : </label>
            <input name="start_date" id="start_date_edit" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="end_date">End Date : </label>
            <input name="end_date" id="end_date_edit" type="text" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="description">Event Description : </label>
            <textarea name="description" id="description_edit" class="form-control" style="resize: none;"></textarea>
          </div><br>
          <div class="modal-footer">
            <div class="row">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary">Edit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- End Calendar Modal -->
