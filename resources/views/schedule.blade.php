@extends('layouts.app')
@section('css')
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-datetimepicker.min.css')}}' />
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-glyphicons.css')}}' />
@endsection
@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
          <div class="panel-body">
            <h4>Schedules</h4>
            <table id="tablesched" class="table table-striped">
              <thead>
                <tr>
                  <th>Teacher</th>
                  <th>Subject</th>
                  <th>Grade</th>
                  <th>Strand</th>
                  <th>Day</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sched as $schedules)
                  <tr>
                    <td>{{ $schedules->myTeacher->firstname }} {{ $schedules->myTeacher->middlename }} {{ $schedules->myTeacher->lastname }}</td>
                    <td>{{ $schedules->subject->subject_name }}</td>
                    <td>{{ $schedules->strand->grade }}</td>
                    <td>{{ $schedules->strand->strand }}</td>
                    <td>{{ $schedules->day }}</td>
                    <td>{{ date('h:i A' , strtotime($schedules->time_start)) }}</td>
                    <td>{{ date('h:i A' , strtotime($schedules->time_end)) }}</td>
                    <td>  <button type="button" id="editbutton" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit{{ $schedules->id }}" >
                      Edit </button> 
                      <!-- Edit Modal-->
                      <div id="edit{{ $schedules->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit Schedule</h4>
                            </div>
                            <form method="POST" action="{{ route('editSched') }}" id="editformsched">
                              @csrf
                              <input type="hidden" name="id" value="{{ $schedules->id }}">
                               <div class="modal-body">
                                <div class="form-group">
                                  <label for="teacher_profile_id">Teacher</label>
                                  <select id = "selectteacher" name="teacher_profile_id">
                                    @foreach($teach->where('teacher_id', $schedules->teacher_profile_id) as $teacher)
                                    <option value={{ $teacher->teacher_id }} selected> {{ $teacher->firstname }} {{ $teacher->middlename }} {{ $teacher->lastname }} </option>
                                    @endforeach
                                    @foreach($teach->where('teacher_id', '!=', $schedules->teacher_profile_id) as $notteacher)
                                    <option value={{ $notteacher->teacher_id }} > {{ $notteacher->firstname }} {{ $notteacher->middlename }} {{ $notteacher->lastname }} </option>
                                    @endforeach                               
                                  </select>
                                  
                                </div>
                                <div class="form-group">
                                  <label for="subject_id">Subject</label>
                                  <select name="subject_id" id="subjects">
                                    @foreach($subj->where('id', $schedules->subject_id) as $subjects)
                                      <option value={{ $subjects->id }} selected > {{ $subjects->subject_name }}</option>
                                    @endforeach
                                    @foreach($subj->where('id', '!=', $schedules->subject_id)  as $notsubjects)
                                      <option value={{ $notsubjects->id }}> {{ $notsubjects->subject_name }}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="grade">Grade: </label>
                                  <select name="grade" class="form-control">
                                    @if ($schedules->grade == 11)
                                 {!! '<option value=11 selected> Grade 11</option>
                                      <option value=12> Grade 12</option>' !!}
                              @elseif ($schedules->grade == 12)
                                {!! '<option value=11> Grade 11</option>
                                     <option value=12 selected> Grade 12</option>' !!}
                                @endif
                                      
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="strand_id">Strand</label>
                                  <select name="strand_id">
                                    @foreach($strand->where('id', $schedules->strand_id) as $strands)
                                      <option value={{ $strands->id }} > Grade {{ $strands->grade }} {{ $strands->strand }}</option>
                                    @endforeach
                                    @foreach($strand->where('id','!=', $schedules->strand_id) as $strands)
                                      <option value={{ $strands->id }} > Grade {{ $strands->grade }} {{ $strands->strand }}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="day">Day</label>
                                  <select name="day" id="schedday">
                                    @switch($schedules->day)
                                    @case("Monday")
                                       <option value="Monday" selected>Monday</option>
                                       <option value="Tuesday">Tuesday</option>
                                       <option value="Wednesday">Wednesday</option>
                                       <option value="Thursday">Thursday</option>
                                       <option value="Friday">Friday</option>
                                        @break
                                    @case("Tuesday")
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday" selected>Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        @break
                                    @case("Wednesday")
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday" selected>Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        @break
                                    @case("Thursday")
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday" selected>Thursday</option>
                                        <option value="Friday">Friday</option>
                                        @break
                                    @case("Friday")
                                       <option value="Monday">Monday</option>
                                       <option value="Tuesday">Tuesday</option>
                                       <option value="Wednesday">Wednesday</option>
                                       <option value="Thursday">Thursday</option>
                                       <option value="Friday" selected>Friday</option>
                                        @break
                                    @default
                                        <span>Something went wrong, please try again</span>
                                  @endswitch
                                  </select>
                                </div>
                                <label for="time_start">Time Start</label>
                                  <input name="time_start" type='time' class="form-control" value="{{ $schedules->time_start }}">
                                
                                <label for="time_end">Time End</label>
                                  <input name="time_end" type='time' class="form-control" value="{{ $schedules->time_end }}">
                                  
                               
                                
                              </div>
                              <div class="form-group">
                                <label for="semester">Semester </label>
                                <select name="semester" class="form-control">
                                   @if ($schedules->semester == 1)
                                   {!! '<option value=1 selected> First Semester</option>
                                        <option value=2> Second Semester</option>' !!}
                                @elseif ($schedules->semester == 2)
                                  {!! '<option value=1> First Semester</option>
                                       <option value=2 selected> Second Semester</option>' !!}
                                  @endif
                                </select>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-default editsched" >Edit </button>
                              </div>

                            </div>
                          </form>
                        </div>
                      </div>
                      <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" id="deletesubject" data-id="{{ $schedules->id }}"data-target="#{{ $schedules->id }}"><span class="glyphicon glyphicon-trash"></span> </button>

                      <!-- Delete Modal -->
                      <div id="{{ $schedules->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Delete Confirmation</h4>
                            </div>
                            <div class="modal-body">
                              <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                              <form method="POST" action="{{ route('deleteSched') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $schedules->id }}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger">Delete</button>
                              </form>
                            </div>
                          </div>

                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach

              </table>

              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddSchedule">Add Schedule</button>
              <!-- Modal -->
              <div id="AddSchedule" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Add Schedule</h4>
                    </div>
                    <form method="POST" action="#" id="submitformsched">
                      @csrf
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="teacher_profile_id">Teacher : </label>
                          <select name="teacher_profile_id" class="form-control">
                            @foreach($teach as $teacher)
                              <option value={{ $teacher->teacher_id }}> {{ $teacher->firstname }} {{ $teacher->middlename }} {{ $teacher->lastname }} </option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="subject_id">Subject : </label>
                          <select name="subject_id" class="form-control">
                            @foreach($subj as $subjects)
                              <option value={{ $subjects->id }}> {{ $subjects->subject_name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="grade">Grade </label>
                          <select name="grade" class="form-control">
                              <option value=11> Grade 11</option>
                              <option value=12> Grade 12</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="strand_id">Strand : </label>
                          <select name="strand_id" class="form-control">
                            @foreach($strand as $strands)
                              <option value={{ $strands->id }} > Grade {{ $strands->grade }} {{ $strands->strand }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="day">Day : </label>
                          <select name="day" class="form-control">
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                            <option value="Friday">Friday</option>
                          </select>
                        </div>
                        <label for="time_start">Time Start</label>
                        <div class='input-group date' id='datetimepicker1'>
                          <input name="time_start" type='text' class="form-control" />
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                        <label for="time_end">Time End</label>
                        <div class='input-group date' id='datetimepicker2'>
                          <input name="time_end" type='text' class="form-control" />
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                        <div class="form-group">
                          <label for="semester">Semester </label>
                          <select name="semester" class="form-control">
                              <option value=1> First Semester</option>
                              <option value=2> Second Semester</option>
                          </select>
                        </div>
                        <!-- <div class="form-group">
                        <label for="time_start">Time Start</label>
                        <input type="time" name="time_start">
                        @if ($errors->has('time_start'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('time_start') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                  <label for="time_end">Time End</label>
                  <input type="time" name="time_end">
                  @if ($errors->has('time_end'))
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('time_end') }}</strong>
                </span>
              @endif
            </div> -->
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary submitsched" >Add New Schedule</button>
          </div>

        </div>
      </form>
    </div>
  </div>

</div>
</div>
</div>
</div>
</div>

@endsection

@section('js')
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/moment.min.js')}}"></script>
  <script>
  $(document).ready(function() {
    $('#datetimepicker1').datetimepicker({
      format: 'LT'
    });
    $('#datetimepicker2').datetimepicker({
      format: 'LT'
    });
    $('#datetimepicker3').datetimepicker({
      format: 'LT'
    });
    $('#datetimepicker4').datetimepicker({
      format: 'LT'
    });

  })
</script>
<script>
$( ".submitsched" ).click(function(e) {
  e.preventDefault();
  $('.modal').modal('hide');
  $('#processing').modal('show')
  setTimeout(function() {
    $('#processing').modal('hide');
  }, 897);
  $.ajax({
    type: "POST",
    url: "{{ route('addSched') }}",
    data: $("#submitformsched").serialize(),
    success: function(store) {
      $( "#tablesched" ).load( "{{ route('schedule') }} #tablesched" );
      $("#submitformsched").trigger("reset");
    },
    error: function() {
    },
  });
});
</script>
<script>
$( ".editsched" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "{{ route('editSched') }}",
      data: $("#editformsched").serialize(),
      success: function(store) {
        $( "#tablesched" ).load( "{{ route('schedule') }} #tablesched" );
        $("#editformsched").trigger("reset");
      },
      error: function() {
      },
    });
  });

});
</script>
<script type="text/javascript">
</script>

@endsection
