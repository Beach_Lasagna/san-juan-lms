@extends('layouts.app')


@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
          <div class="panel-body">
            <h4>Senior High School Subjects</h4>
            <table id="tablesubject" class="table tabe-striped">
              <thead>
                <tr>
                  <th>Subject Name</th>
                  <th>Action</th>
                  <!--<th>Strand</th>
                  <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
                @foreach($subj as $subjects)
                  <tr>
                    <td>{{ $subjects->subject_name }}</td>
                    <td> <button type="button" class="btn btn-default btn-lg" data-id="{{ $subjects->id }}" data-toggle="modal"  data-target="#edit{{ $subjects->id }}"><span class="glyphicon glyphicon-pencil"></span></button>

                      <!-- Edit Modal-->

                      <div id="edit{{ $subjects->id }}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit Subject</h4>
                            </div>
                            <div class="modal-body">
                              <form method="POST" action="{{ route('editSubject') }}" id="editform" autocomplete="off">
                                @csrf
                                <label for="subject_name">Subject Name : </label>
                                <input type="hidden" name="id" value="{{ $subjects->id }}">
                                <input type="text" name="subject_name" value="{{ $subjects->subject_name }}" class="form-control">
                                @if ($errors->has('subject_name'))
                                  <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('subject_name') }}</strong>
                                  </span>
                                @endif
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit"  class="btn btn-primary editsubject" >Edit</button>
                              </div>
                            </form>
                          </div>

                        </div>
                      </div>
                      <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-id="{{ $subjects->id }}" data-target="#{{ $subjects->id }}"><span class="glyphicon glyphicon-trash"> </button>
                        <!-- Delete Modal -->
                        <div id="{{ $subjects->id }}" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete Confirmation</h4>
                              </div>
                              <div class="modal-body">
                                <p>Are you sure you want to delete this?</p>
                              </div>
                              <form action="{{ route('deleteSubject') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $subjects->id }}">
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                  <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                              </form>
                            </div>

                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddSubject">Add Subject</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div id="AddSubject" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Subject</h4>
          </div>
          <div class="modal-body">
            <form method="POST" action="#" id="subjectform" autocomplete="off">
              @csrf
              <label for="subject_name">Subject Name : </label>
              <input type="text" name="subject_name" class="form-control">
              @if ($errors->has('subject_name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('subject_name') }}</strong>
                </span>
              @endif
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit"  class="btn btn-primary submitsubject" >Add</button>
            </div>
          </form>
        </div>

      </div>
    </div>





  @endsection

  @section('js')
    <script>

    $( ".submitsubject" ).click(function(e) {
      e.preventDefault();
      $('.modal').modal('hide');
      $('#processing').modal('show')
      setTimeout(function() {
        $('#processing').modal('hide');
      }, 2000);
      $.ajax({
        type: "POST",
        url: "{{ route('addSubject') }}",
        data: $("#subjectform").serialize(),
        success: function(store) {
          $( "#tablesubject" ).load( "{{ route('subject') }} #tablesubject" );
          $("#subjectform").trigger("reset");
        },
        error: function() {
        },
      });
    });
  </script>

@endsection
