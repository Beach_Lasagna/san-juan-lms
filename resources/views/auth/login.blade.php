@extends('layouts.app')

@section('css')
<style>
  body {
    background-image: url({{asset('images/login-bg.jpg')}});
    background-repeat: no-repeat;
    background-size: cover;
  }
  .login-box {
    margin-top: 300px !important;
  }
</style>

@endsection

@section('content')
  <div class="container">
    <div class="login-box margin-top-md panel">
      <div class="login-box-body center panel-body">

        <h3 align="center">Log In</h3>
        <form method="POST" action="{{ route('login') }}">
          @csrf
          <label for="email">Email</label>
          <div class="form-group has-feedback"">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autocomplete="off" required autofocus>
            <span class="lnr lnr-envelope form-control-feedback" style="font-size: 25px;"></span>
            @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
          <label for="password">Password</label>
          <div class="form-group has-feedback"">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required><span style="font-size: 25px;" class="lnr lnr-lock form-control-feedback"></span>
            @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
          <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          <label class="form-check-label" for="remember">
            {{ __('Remember Me') }}
          </label>
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
