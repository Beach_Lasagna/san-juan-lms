@extends('layouts.app')


@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="panel">
        <div class="panel-body">
          <h4>Teachers</h4>
          <table id="tableteacher" class="table table-striped">
            <thead>
              <tr>
                <th>Lastname</th>
                <th>Firstname</th>
                <th>Middlename</th>
                <th>Position</th>
                <th>Address</th>
                <th>Age</th>
                <th>Birthday</th>
                <th>School Graduated</th>
                <th>Year Graduated</th>
                <th>Educational Attainment</th>
                <th>Subjects</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if(count($teach) > 0)
                @foreach($teach as $teacher)
                  <tr>
                    <td>{{ $teacher->lastname }}</td>
                    <td>{{ $teacher->firstname }}</td>
                    <td>{{ $teacher->middlename }}</td>
                    <td>{{ $teacher->position }}</td>
                    <td>{{ $teacher->address }}</td>
                    <td>{{ $teacher->age }}</td>
                    <td>{{ $teacher->birthmonth }} {{ $teacher->birthday }} ,
                      {{ $teacher->birthyear }}</td>
                      <td>{{ $teacher->school_graduated }}</td>
                      <td>{{ $teacher->year_graduated }}</td>
                      <td>{{ $teacher->educational_attainment }}</td>
                      <td>{{ $teacher->subjects }}</td>
                      <td>{{ $teacher->contact }}</td>
                      <td>{{ $teacher->email }}</td>
                      <td>  <button type="button" class="btn btn-default btn-lg editteachermodal" data-toggle="modal"   data-target="#edit{{ $teacher->teacher_id }}"><span class="glyphicon glyphicon-pencil"> </button>
                        <!-- Edit Modal-->
                        <div id="edit{{ $teacher->teacher_id }}" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Edit Teacher Information</h4>
                              </div>
                              <div class="modal-body">
                                <form method="POST" action="{{ route('editTeacher')}}" id="editformteach">
                                  @csrf
                                  <input type="hidden" name="teacher_id" value="{{ $teacher->teacher_id }}">
                                  <input type="hidden" name="user_id" value="{{ $teacher->user_id }}">
                                  <div class="form-group">
                                    <label for="firstname">Firstname : </label>
                                    <input type="text" name="firstname" value="{{ $teacher->firstname }}" class="form-control">
                                    @if ($errors->has('firstname'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="middlename">Middlename : </label>
                                    <input type="text" name="middlename" value="{{ $teacher->middlename }}" class="form-control">
                                    @if ($errors->has('middlename'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('middlename') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="lastname">Lastname : </label>
                                    <input type="text" name="lastname" value="{{ $teacher->lastname }}" class="form-control">
                                    @if ($errors->has('lastname'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="position">Position : </label>
                                    <input type="text" name="position" value = "{{ $teacher->position }}" class="form-control">
                                    @if ($errors->has('position'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('position') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="address">Address : </label>
                                    <input type="text" name="address" value = "{{ $teacher->address }}" class="form-control">
                                    @if ($errors->has('address'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="age">Age : </label>
                                    <input type="number" name="age" value = "{{ $teacher->age }}" class="form-control">
                                    @if ($errors->has('age'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('age') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="birthmonth">Birthmonth : </label>
                                    <input type="text" name="birthmonth" value = "{{ $teacher->birthmonth }}" class="form-control">
                                    @if ($errors->has('birthmonth'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthmonth') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="birthday">Birthday : </label>
                                    <input type="number" name="birthday" value = "{{ $teacher->birthday}}" class="form-control">
                                    @if ($errors->has('birthday'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="birthyear">Birthyear : </label>
                                    <input type="number" name="birthyear" value = "{{ $teacher->birthyear }}" class="form-control">
                                    @if ($errors->has('birthyear'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthyear') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="school_graduated">School Graduated : </label>
                                    <input type="text" name="school_graduated" value = "{{ $teacher->school_graduated }}" class="form-control">
                                    @if ($errors->has('school_graduated'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('school_graduated') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="year_graduated">Year Graduated : </label>
                                    <input type="number" name="year_graduated" value = "{{ $teacher->year_graduated }}" class="form-control">
                                    @if ($errors->has('year_graduated'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('year_graduated') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="educational_attainment">Educational Attainment : </label>
                                    <input type="text" name="educational_attainment" value = "{{ $teacher->educational_attainment }}" class="form-control">
                                    @if ($errors->has('educational_attainment'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('educational_attainment') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="subjects">Subject/s Taught : </label>
                                    <input type="text" name="subjects" value = "{{ $teacher->subjects }}" class="form-control">
                                    @if ($errors->has('subjects'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('subjects') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="contact">Contact Number : </label>
                                    <input type="text" name="contact" value = "{{ $teacher->contact }}" class="form-control">
                                    @if ($errors->has('contact'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <div class="form-group">
                                    <label for="email">Email Address : </label>
                                    <input type="email" name="email" value = "{{ $teacher->email }}" class="form-control">
                                    @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                                  <!-- <label for="password">Password</label>
                                  <input type="password" name="password">
                                  @if ($errors->has('password'))
                                  <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                                </span>
                              @endif
                              <input type="hidden" name="usertype" value="Faculty"> -->
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit"  class="btn btn-primary updateteacher" >Edit</button>
                            </div>
                          </form>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                  <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-id="{{ $teacher->teacher_id }}" id="deletesubject" data-target="#{{ $teacher->teacher_id }}"><span class="glyphicon glyphicon-trash"> </button>

                  <!-- Delete Modal -->
                  <div id="{{ $teacher->teacher_id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Delete Confirmation</h4>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete this?</p>
                        </div>
                        <form action="{{ route('deleteTeacher') }}" method="POST">
                          @csrf
                          <input type="hidden" name="teacher_id" value="{{ $teacher->teacher_id }}">
                          <input type="hidden" name="user_id" value="{{ $teacher->user_id }}">
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Delete</button>
                          </div>
                        </form>
                      </div>

                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>

      <br><br><br>

      <!-- Trigger the modal with a button -->
      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#AddTeacher">Add Teacher</button>

      <!--Add Modal -->
      <div id="AddTeacher" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Teacher Information</h4>
            </div>
            <div class="modal-body">
              <form method="POST" action="#" id="submitformteach">
                @csrf
                <div class="form-group">
                  <label for="firstname">Firstname : </label>
                  <input type="text" name="firstname" class="form-control">
                  @if ($errors->has('firstname'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('firstname') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="middlename">Middlename : </label>
                  <input type="text" name="middlename" class="form-control">
                  @if ($errors->has('middlename'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('middlename') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="lastname">Lastname : </label>
                  <input type="text" name="lastname" class="form-control">
                  @if ($errors->has('lastname'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="position">Position : </label>
                  <input type="text" name="position" class="form-control">
                  @if ($errors->has('position'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('position') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="address">Address : </label>
                  <input type="text" name="address" class="form-control">
                  @if ($errors->has('address'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('address') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="age">Age : </label>
                  <input type="number" name="age" class="form-control">
                  @if ($errors->has('age'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('age') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="birthmonth">Birthmonth : </label>
                  <input type="text" name="birthmonth" class="form-control">
                  @if ($errors->has('birthmonth'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('birthmonth') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="birthday">Birthday : </label>
                  <input type="number" name="birthday" class="form-control">
                  @if ($errors->has('birthday'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('birthday') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="birthyear">Birthyear : </label>
                  <input type="number" name="birthyear" class="form-control">
                  @if ($errors->has('birthyear'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('birthyear') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="school_graduated">School Graduated : </label>
                  <input type="text" name="school_graduated" class="form-control">
                  @if ($errors->has('school_graduated'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('school_graduated') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="year_graduated">Year Graduated : </label>
                  <input type="number" name="year_graduated" class="form-control">
                  @if ($errors->has('year_graduated'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('year_graduated') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="educational_attainment">Educational Attainment : </label>
                  <input type="text" name="educational_attainment" class="form-control">
                  @if ($errors->has('educational_attainment'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('educational_attainment') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="subjects">Subject/s Taught : </label>
                  <input type="text" name="subjects" class="form-control">
                  @if ($errors->has('subjects'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('subjects') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="contact">Contact Number : </label>
                  <input type="text" name="contact" class="form-control">
                  @if ($errors->has('contact'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('contact') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="email">Email Address : </label>
                  <input type="email" name="email" class="form-control">
                  @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
                <!-- <label for="password">Password</label>
                  <input type="password" name="password">
                  @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                  <input type="hidden" name="usertype" value="Faculty"> -->
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit"  class="btn btn-primary submitteacher" >Add</button>
                </div>
              </form>
            </div>

          </div>
        </div>


      @endsection
      <!-- Trigger the modal with a button -->

      @section('js')
        <script>
          $( ".submitteacher" ).click(function(e) {
            e.preventDefault();
            $('.modal').modal('hide');
            $('#processing').modal('show')
            setTimeout(function() {
              $('#processing').modal('hide');
            }, 2000);
            $.ajax({
              type: "POST",
              url: "{{ route('addTeacher') }}",
              data: $("#submitformteach").serialize(),
              success: function(store) {
                $( "#tableteacher" ).load( "{{ route('teacher') }} #tableteacher" );
                $("#submitformteach").trigger("reset");
              },
              error: function() {
              },
            });
          });
        </script>

      @endsection
