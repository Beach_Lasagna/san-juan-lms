@extends('layouts.app')
@section('css')
  <style type="text/css">
  @include('switchstyle')
</style>
@endsection
@section('content')
  <div class="main">
    <div class="main-content">
      <div class="container-fluid">
        @if (session()->has('success'))
          <div class="alert alert-success">
            <p>{{session('success')}}</p>
          </div>
        @endif
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
          </div>
          <div class="panel-body">
            @foreach($setting as $set)
              <div class="row">
                <div class="col-md-4">
                  <h4>School Year</h4>
                </div>
                <div class="col-md-2">
                  <h4>{{$set->school_year}}</h4>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                  @if ($set->semester == 1)
                    {!! '<h4 data-toggle="modal" data-target="#semestermodal">We are currently in the first semester</h4>' !!}
                  @elseif ($set->semester == 2)
                    {!! '<h4 data-toggle="modal" data-target="#semestermodal">We are currently in the second semester</h4>' !!}
                  @endif
                </div>
                <div class="col-md-2">
                  <button class="btn btn-primary" id="endSemester">End Semester</button>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                  @if ($set->enrollment == 1)
                    {!! '<h4>Enrollment is ongoing</h4>' !!}
                  @elseif ($set->enrollment == 0)
                    {!! '<h4>Enrollment is close</h4>' !!}
                  @endif
                </div>
                <div class="col-md-2">
                  <label class="switch">
                    <input name="enrollment"  data-toggle="modal" data-target="#enrollmodal" id="enrollment" type="checkbox" value="{{ $set->enrollment }}">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                  @if ($set->encoding == 1)
                    {!! '<h4>Encoding period is ongoing</h4>' !!}
                  @elseif ($set->encoding == 0)
                    {!! '<h4>Encoding period has finished</h4>' !!}
                  @endif
                </div>
                <div class="col-md-2">
                  <label class="switch">
                    <input name="encoding" data-toggle="modal" data-target="#encodemodal" id="encoding" type="checkbox" value={{ $set->encoding }}>
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>

              <!-- enrollment modal-->
              <div class="modal fade" id="enrollmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h3 class="modal-title" id="exampleModalLongTitle">Alert</h3>
                    </div>
                    <div class="modal-body">
                      <div class="alert alert-warning">
                        <form class="theform" method="POST" action="{{ route('editSettings') }}">
                          @csrf
                          <input type="hidden" name="id" value="{{ $set->id }}">
                          @if ($set->enrollment == 1)
                            {!! '<input type="hidden" name="enrollment" value= 0>Are you sure you want to finish enrollment period?' !!}
                          @elseif ($set->enrollment == 0)
                            {!! '<input type="hidden" name="enrollment" value= 1>Are you sure you want to start enrollment status?' !!}
                          @endif
                        </div>
                        <input type="hidden" name="encoding" value="{{ $set->encoding }}">
                        <input type="hidden" name="semester" value="{{ $set->semester }}">
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-success" >Yes</button>
                          <button type="submit" data-dismiss="modal" class="btn btn-danger" onclick="location.reload();" >Not yet..</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <!--ENCODE MODAL-->
              <div class="modal fade" id="encodemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h3 class="modal-title" id="exampleModalLongTitle">Alert</h3>
                    </div>
                    <div class="modal-body">
                      <div class="alert alert-warning">
                        <form class="theform" method="POST" action="{{ route('editSettings') }}">
                          @csrf
                          <input type="hidden" name="id" value="{{ $set->id }}">
                          @if ($set->encoding == 1)
                            {!! '<input type="hidden" name="encoding" value= 0>Are you sure you want to finish encoding period?' !!}
                          @elseif ($set->encoding == 0)
                            {!! '<input type="hidden" name="encoding" value= 1>Are you sure you want to start encoding period?' !!}
                          @endif
                        </div>
                        <input type="hidden" name="enrollment" value="{{ $set->enrollment }}">
                        <input type="hidden" name="semester" value="{{ $set->semester }}">
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-success" >Yes</button>
                          <button type="submit" data-dismiss="modal" class="btn btn-danger" onclick="location.reload();" >Not yet..</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- modals -->
  <div class="modal fade" id="endSemesterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Confirm End Semester</h3>
        </div>
        <div class="modal-body">
          <div class="alert alert-warning">
            <p>Are you sure you want to end semester?</p>
            <p>This action cannot be undone.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <a class="btn btn-primary" href="/admin/endsem">Submit</a>
        </div>
      </div>
    </div>
  </div>


@endsection
@section('js')
  <script>
  var $enrollment  = $('#enrollment').val();
  var $enrollment2 = parseInt($enrollment);
  var $newstatus = (1 - $enrollment2);

  if( $enrollment == 1){
    $('#enrollment').val(1);
    $( "#enrollment" ).prop( "checked", true );
  }
  else if( $enrollment == 0){
    $('#enrollment').val(0);
    $( "#enrollment" ).prop( "checked", false );
  }
  // $("#enrollment").click(function(){
  //    $('#enroll').submit();
  // });
  // $("#enroll").on('submit', function() {
  //    $('#enrollment').val($newstatus);
  // });


  </script>

  <script>
  var $encoding  = $('#encoding').val();
  if( $encoding == 1){
    $('#encoding').val(1);
    $( "#encoding" ).prop( "checked", true );
  }
  else if( $encoding == 0){
    $('#encoding').val(0);
    $( "#encoding" ).prop( "checked", false );
  }

  $("#endSemester").click(function() {
    $("#endSemesterModal").modal('toggle');
  });
  // $("#encoding").click(function(){
  // 	if( $encoding == 0){
  //     $('#encoding').val(1);
  //     $( "#encoding" ).prop( "checked", true );
  // }
  //  if( $encoding == 1){
  // 	$('#encoding').val(0);
  // 	$( "#encoding" ).prop( "checked", false );
  // }
  // });
  </script>
  <script>

  </script>
@endsection
