@extends('layouts.app')

@section('css')
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-datetimepicker.min.css')}}' />
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-glyphicons.css')}}' />
@endsection

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <div class="pull-right">
              <ul class="list-inline">
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/strand/{{$sched->strand_id}}/{{$sched->id}}"><h4>Student List</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/grading/{{$sched->strand_id}}/{{$sched->id}}"><h4>Grades</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/activity/{{$sched->strand_id}}/{{$sched->id}}"><h4>Activities</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$sched->strand_id}}/{{$sched->id}}"><h4>Exams</h4></a></li>
              </ul>
            </div>
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              {{$sched->subject->subject_name}} <br>
              {{date('g:i A' ,strtotime($sched->time_start))}} - {{date('g:i A' ,strtotime($sched->time_end))}}
            </p>
          </div>
          <div class="panel-body">
            <div class="row">
              <a class="btn btn-primary" data-toggle="modal" data-target="#createExamModal">Create Exam</a>
              @if (count($exams) > 0)
                <table class="table table-striped" id="examtable">
                  <thead>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Date Published</th>
                    <th>Deadline</th>
                    <th>Actions</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </thead>
                  @foreach ($exams as $exam)
                    <tbody>
                      <td>{{$exam->title}}</td>
                      <td>{{$exam->description}}</td>
                      <td>{{date('M j Y g:i A' ,strtotime($exam->created_at))}}</td>
                      <td>{{date('M j Y g:i A' ,strtotime($exam->deadline))}}</td>
                      <td><a href="/teacher/exam/questions/{{$strand->id}}/{{$sched->id}}/{{$exam->id}}/results" class="btn btn-success">Student's Answers</a></td>
                      <td><a href="/teacher/exam/questions/{{$strand->id}}/{{$sched->id}}/{{$exam->id}}" class="btn btn-success">Questions</a></td>
                      <td><a id="editExam-{{$exam->id}}" class="btn btn-warning">Edit</a></td>
                      <td>
                        <form action="{{url('/teacher/exam/delete/' . $exam->id)}}" method="post" autocomplete="off" enctype="multipart/form-data">
                          @csrf
                          <button class="btn btn-danger">Delete</button>
                        </form>
                      </td>
                    </tbody>
                  @endforeach
                </table>
              @else
                <p>No Exams Found.</p>
              @endif
            </div>
          </div>
          <!-- END OVERVIEW -->
        </div>
      </div>
    </div>
    <!-- END MAIN CONTENT -->
  </div>
  <!-- END MAIN -->

  <!-- Modals -->
  <div class="modal fade" id="createExamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Create Exam</h3>
        </div>
        <div class="modal-body">
          <form action="{{url('/teacher/exam/store/' . $sched->id)}}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="title">Title : </label>
              <input name="title"  type="text" class="form-control" required/>
            </div>
            <div class="form-group">
              <label for="description">Description : </label>
              <textarea name="description"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="deadline">Deadline : </label>
              <div class='input-group date' id='datetimepicker1'>
                <input name="deadline" type='text' class="form-control" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit Activity Modal -->
  <div class="modal fade" id="editExamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Edit Exam</h3>
        </div>
        <div class="modal-body">
          <form id = "editactivitymodalform" action="/teacher/exam/edit" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <input name="exam_id" type="hidden" id="exam_id">
            <div class="form-group">
              <label for="title">Title : </label>
              <input name="title" type="text" class="form-control" id="edit_title" required/>
            </div>
            <div class="form-group">
              <label for="description">Description : </label>
              <textarea name="description" id="edit_description" class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="deadline">Deadline : </label>
              <div class='input-group date' id='datetimepicker2'>
                <input name="deadline" id="edit_deadline" type='text' class="form-control" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary editactivitymodalsubmit">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modals -->
@endsection

@section('js')
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/moment.min.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();

    $('#createExam').click(function () {
      $('#createExamModal').modal('show');
    })

    $("[id^=editExam-]").click(function(e) {
      var exam_id = e.target.id.substring(e.target.id.indexOf("-") + 1);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/teacher/exam/getExam',
        type: 'POST',
        data: {
          examID: exam_id,
        },
        success:function(exam) {
          $('#exam_id').val(exam.id);
          $('#edit_title').val(exam.title);
          $('#edit_description').val(exam.description);
          $('#edit_deadline').val(exam.deadline);
        }
      });
      $('#editExamModal').modal('show');
    })
  });
  </script>

@endsection
