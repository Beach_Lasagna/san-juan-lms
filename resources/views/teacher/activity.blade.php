@extends('layouts.app')

@section('css')
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-datetimepicker.min.css')}}' />
  <link rel='stylesheet' href='{{asset('datetime-picker/build/css/bootstrap-glyphicons.css')}}' />
@endsection

  @section('content')
    <!-- MAIN -->
    <div class="main">
      <!-- MAIN CONTENT -->
      <div class="main-content">
        <div class="container-fluid">
          <!-- OVERVIEW -->
          <div class="panel panel-headline">
            <div class="panel-heading">
              <div class="pull-right">
                <ul class="list-inline">
                  <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/strand/{{$sched->strand_id}}/{{$sched->id}}"><h4>Student List</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/grading/{{$sched->strand_id}}/{{$sched->id}}"><h4>Grades</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/activity/{{$sched->strand_id}}/{{$sched->id}}"><h4>Activities</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$sched->strand_id}}/{{$sched->id}}"><h4>Exams</h4></a></li>
                </ul>
              </div>
              <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
              <p class="panel-subtitle">
                {{$sched->subject->subject_name}} <br>
                {{date('g:i A' ,strtotime($sched->time_start))}} - {{date('g:i A' ,strtotime($sched->time_end))}}
              </p>
            </div>
            <div class="panel-body">
              <div class="row">
                <a class="btn btn-primary" data-toggle="modal" data-target="#addActivityModal">Add Activity</a>
                <table class="table table-striped">
                  <thead>
                    <th></th>
                    <th>Activity Type</th>
                    <th>Attached File</th>
                    <th>Date Published</th>
                    <th>Deadline</th>
                    <th></th>
                    <th></th>
                  </thead>
                  @foreach ($activities as $activity)
                    <thead>
                      <tr>
                        <th><a href="/teacher/activity/view/{{$activity->id}}/{{$sched->id}}"><h4>{{$activity->title}}</h4></a></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$activity->description}}</td>
                        <td>{{$activity->type}}</td>
                        <td>
                          @if($activity->file != '')
                            <a href="{{asset('storage/assignments/' . $activity->file)}}" download>{{$activity->file}}</a>
                          @else
                            <p>No file attached.</p>
                          @endif
                        </td>
                        <td>{{date('M j Y g:i A' ,strtotime($activity->created_at))}}</td>
                        <td>{{date('M j Y g:i A' ,strtotime($activity->deadline))}}</td>
                        <td><a id="editActivity-{{$activity->id}}" class="btn btn-warning">Edit Activity</a></td>
                        <td>
                          <form action="{{url('/teacher/activity/delete/' . $activity->id . '/' . $sched->id)}}" method="post" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <button class="btn btn-danger">Delete Activity</button>
                          </form>
                        </td>
                      </tr>
                    </tbody>
                  @endforeach
                </table>
              </div>
            </div>
            <!-- END OVERVIEW -->
          </div>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- Modals -->
    <div class="modal fade" id="addActivityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLongTitle">Add Activity</h3>
          </div>
          <div class="modal-body">
            <form action="{{url('/teacher/activity/store/' . $sched->id)}}" method="post" autocomplete="off" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="title">Title : </label>
                <input name="title"  type="text" class="form-control" required/>
              </div>
              <div class="form-group">
                <label for="type">Type : </label>
                <select name="type"class="form-control"/>
                <option value="Lesson">Lesson</option>
                <option value="Assignment">Assignment</option>
              </select>
            </div>
            <div class="form-group">
              <label for="description">Description : </label>
              <textarea name="description"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="deadline">Deadline : </label>
              <div class='input-group date' id='datetimepicker1'>
                <input name="deadline" type='text' class="form-control" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="file">Attach a file : </label>
              <input name="file" type="file" class="form-control"/>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit Activity Modal -->
  <div class="modal fade" id="editActivityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Edit Activity</h3>
        </div>
        <div class="modal-body">
          <form action="{{url('/teacher/activity/edit/' . $sched->id)}}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <input name="activity_id" type="hidden" id="activity_id">
            <div class="form-group">
              <label for="title">Title : </label>
              <input name="title" type="text" class="form-control" id="edit_title" required/>
            </div>
            <div class="form-group">
              <label for="type">Type : </label>
              <select name="type"class="form-control"/>
              <option value="Lesson">Lesson</option>
              <option value="Assignment">Assignment</option>
            </select>
          </div>
          <div class="form-group">
            <label for="description">Description : </label>
            <textarea name="description" id="edit_description" class="form-control" style="resize: none;"></textarea>
          </div>
          <div class="form-group">
            <label for="deadline">Deadline : </label>
            <div class='input-group date' id='datetimepicker2'>
              <input name="deadline" id="edit_deadline" type='text' class="form-control" />
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="file">Change file : </label>
            <input name="file" id="edit_file" type="file" class="form-control"/>
          </div>
          <div class="modal-footer">
            <div class="row">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modals -->
@endsection

@section('js')
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('datetime-picker/build/js/moment.min.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();

    $('#addActivity').click(function () {
      $('#addActivityModal').modal('show');
    })
    $("[id^=editActivity-]").click(function(e) {
      var act_id = e.target.id.substring(e.target.id.indexOf("-") + 1);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/teacher/activity/getAct',
        type: 'POST',
        data: {
          actID: act_id,
        },
        success:function(activity) {
          $('#activity_id').val(activity.id);
          $('#edit_title').val(activity.title);
          $('#edit_deadline').val(activity.deadline);
          switch (activity.type) {
            case 'Assignment':
            $("select option[value='Assignment']").attr("selected","selected");
            break;
            case 'Lesson':
            $("select option[value='Lesson']").attr("selected","selected");
            break;
          }
          $('#edit_description').val(activity.description);
        }
      });
      $('#editActivityModal').modal('show');
    })
  });
  </script>
@endsection
