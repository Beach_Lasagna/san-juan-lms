@extends('layouts.app')

@section('css')
@endsection

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <div class="pull-right">
              <ul class="list-inline">
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/strand/{{$sched->strand_id}}/{{$sched->id}}"><h4>Student List</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/grading/{{$sched->strand_id}}/{{$sched->id}}"><h4>Grades</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/activity/{{$sched->strand_id}}/{{$sched->id}}"><h4>Activities</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$sched->strand_id}}/{{$sched->id}}"><h4>Exams</h4></a></li>
              </ul>
            </div>
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              {{$sched->subject->subject_name}} <br>
              {{date('g:i A' ,strtotime($sched->time_start))}} - {{date('g:i A' ,strtotime($sched->time_end))}}
            </p>
            <br>
            <h3 class="panel-title">{{$exam->title}}</h3>
            <p class="panel-subtitle">{{$exam->description}}</p>
            <br><br>
            <a class="btn btn-primary" data-toggle="modal" data-target="#addQuestionModal">Add New Question</a>
          </div>
          <div class="panel-body">
            <div class="row" id='tableHere'>
              @if (count($questions) > 0)
                <table class="table table-striped" id="questionTable">
                  @php ($num = 0)
                  <thead>
                    <th>#</th>
                    <th>Question</th>
                    <th>Choice A</th>
                    <th>Choice B</th>
                    <th>Choice C</th>
                    <th>Choice D</th>
                    <th>Answer</th>
                    <th>Actions</th>
                    <th></th>
                  </thead>
                  @foreach ($questions as $q)
                    <tbody>
                      <td>{{++$num}}</td>
                      <td>{{$q->question}}</td>
                      <td>{{$q->A}}</td>
                      <td>{{$q->B}}</td>
                      <td>{{$q->C}}</td>
                      <td>{{$q->D}}</td>
                      <td>{{$q->answer}}</td>
                      <td><button class="btn btn-warning editQuestion" data-toggle="modal" data-target="#editQuestionModal" value="{{$q->id}}">Edit</button></td>
                      <td><a class="btn btn-danger" data-toggle="modal" data-target="#deleteQuestionModal" id='deleteQuestion-{{$q->id}}'>Delete</a></td>
                    </tbody>
                  @endforeach
                </table>
              @else
                <p>No Questions Found.</p>
              @endif
            </div>
          </div>
          <!-- END OVERVIEW -->
        </div>
      </div>
    </div>
    <!-- END MAIN CONTENT -->
  </div>
  <!-- END MAIN -->

  <!-- Modals -->
  <div class="modal fade" id="addQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add Question</h3>
        </div>
        <div class="modal-body">
          <form action="#" method="POST" autocomplete="off" enctype="multipart/form-data" id="storeQuestion">
            @csrf
            <input type="hidden" name="exam_id" value="{{$exam->id}}"/>
            <div class="form-group">
              <label for="question">Question : </label>
              <textarea name="question"  class="form-control" style="resize: none;" required></textarea>
            </div>
            <div class="form-group">
              <label for="a">Choice A : </label>
              <textarea name="a"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="b">Choice B : </label>
              <textarea name="b"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="c">Choice C : </label>
              <textarea name="c"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="d">Choice D : </label>
              <textarea name="d"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="answer">Answer : </label>
              <select name="answer" class="form-control">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
              </select>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary addquestionbtn">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit Activity Modal -->
  <div class="modal fade" id="editQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Edit Question</h3>
        </div>
        <div class="modal-body">
          <form action="#" method="POST" autocomplete="off" enctype="multipart/form-data" id="storeEditedQuestion">
            @csrf
            <input type="hidden" name="question_id" id="question_id"/>
            <div class="form-group">
              <label for="question">Question : </label>
              <textarea name="question" id="edit_question"  class="form-control" style="resize: none;" required></textarea>
            </div>
            <div class="form-group">
              <label for="a">Choice A : </label>
              <textarea name="a" id="edit_a"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="b">Choice B : </label>
              <textarea name="b" id="edit_b"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="c">Choice C : </label>
              <textarea name="c" id="edit_c"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="d">Choice D : </label>
              <textarea name="d" id="edit_d"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="answer">Answer : </label>
              <select name="answer" id="edit_answer" class="form-control">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
              </select>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary editquestionbtn">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- delete Activity Modal -->
  <div class="modal fade" id="deleteQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Delete Question</h3>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
          <form action="#" method="POST" autocomplete="off" enctype="multipart/form-data" id="deleteQuestionFom">
            @csrf
            <input type="hidden" name="question_id_del" id="question_id_del"/>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary deleteactivity">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modals -->
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function() {
    $('#addQuestion').click(function () {
      $('#addQuestionModal').modal('show');
    });

    $("[id^=deleteQuestion-]").click(function (e) {
      $("#question_id_del").val(e.target.id.substring(e.target.id.indexOf("-") + 1));
    });
  });

  </script>
  <script type="text/javascript">
  $(".editQuestion").click(function(e) {
    // var question_id = e.target.id.substring(e.target.id.indexOf("-") + 1);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/teacher/exam/questions/edit',
      type: 'POST',
      data: {
        questionID: $(this).val(),
      },
      success:function(question) {
        $('#question_id').val(question.id);
        $('#edit_question').val(question.question);
        $('#edit_a').val(question.A);
        $('#edit_b').val(question.B);
        $('#edit_c').val(question.C);
        $('#edit_d').val(question.D);
        switch (question.answer) {
          case 'A':
          $("select option[value='A']").attr("selected","selected");
          break;
          case 'B':
          $("select option[value='B']").attr("selected","selected");
          break;
          case 'C':
          $("select option[value='C']").attr("selected","selected");
          break;
          case 'D':
          $("select option[value='D']").attr("selected","selected");
          break;
        }
      }
    });
  });
  </script>
  <script>
  $( ".addquestionbtn" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "/teacher/exam/questions/store",
      data: $("#storeQuestion").serialize(),
      success: function(store) {
        $( "#questionTable" ).load( "/teacher/exam/questions/{{ $strand->id }}/{{ $sched->id }}/{{$exam->id}} #questionTable" );
        
      },
      error: function() {
      },
    });
  });
  $( ".editquestionbtn" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "/teacher/exam/questions/edit/store",
      data: $("#storeEditedQuestion").serialize(),
      success: function(store) {
        $( "#questionTable" ).load( "/teacher/exam/questions/{{ $strand->id }}/{{ $sched->id }}/{{$exam->id}} #questionTable" );
        
      },
      error: function() {
      },
    });
  });
  $( ".deleteactivity" ).click(function(e) {
    e.preventDefault();
    $('.modal').modal('hide');
    $('#processing').modal('show');
    setTimeout(function() {
      $('#processing').modal('hide');
    }, 987);
    $.ajax({
      type: "POST",
      url: "/teacher/exam/questions/delete",
      data: $("#deleteQuestionFom").serialize(),
      success: function(store) {
        $( "#questionTable" ).load( "/teacher/exam/questions/{{ $strand->id }}/{{ $sched->id }}/{{$exam->id}} #questionTable" );
        
      },
      error: function() {
      },
    });
  });
</script>
@endsection
