@extends('layouts.app')

@section('css')
  <style type="text/css">
  .modal-con{
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
  }

  .sModal-content{
    background-color: #FEFEFE;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 650px;
  }

  .close1, .close3, .close4{
    color: black;
    float: right;
    font-size: 150%;
    font-weight: bold;
    margin-right: 2% ;
    margin-top: -2%;
  }
  .close1:hover, .close1:focus, .close3:hover, .close3:focus, .close4:hover, .close4:focus{
    color: red;
    text-decoration: none;
    cursor: pointer;
  }
  .mod-line{
    font-weight: bold;
  }
  </style>
@endsection

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <div class="pull-right">
              <ul class="list-inline">
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/strand/{{$sched->strand_id}}/{{$sched->id}}"><h4>Student List</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/grading/{{$sched->strand_id}}/{{$sched->id}}"><h4>Grades</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/activity/{{$sched->strand_id}}/{{$sched->id}}"><h4>Activities</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$sched->strand_id}}/{{$sched->id}}"><h4>Exams</h4></a></li>
              </ul>
            </div>
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              {{$sched->subject->subject_name}} <br>
              {{date('g:i A' ,strtotime($sched->time_start))}} - {{date('g:i A' ,strtotime($sched->time_end))}}
            </p>
          </div>
          <div class="panel-body" id="tableHere">
            <div class="row">
              @if($setting->encoding == 0)
                <div class="alert alert-danger">
                  <p>Encoding is not yet open.</p>
                </div>
              @endif
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>LRN</th>
                    <th>Surname</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Final Grade</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($students as $student)
                    <tr>
                      <td>{{$student->lrn}}</td>
                      <td>{{$student->lastname}}</td>
                      <td>{{$student->firstname}}</td>
                      <td>{{$student->middlename}}</td>
                      @if (count($student->grades->where('semester', "$setting->semester")->where('grade', $student->grade)->first()->grades->where('subject_id', $sched->subject_id)) > 0)
                        <td>
                          {{$student->grades->where('semester', "$setting->semester")->where('grade', $student->grade)->first()->grades->where('subject_id', $sched->subject_id)->first()->grade}}
                        </td>
                        @if($student->grades->where('semester', "$setting->semester")->where('grade', $student->grade)->first()->grades->where('subject_id', $sched->subject_id)->first()->remarks == "Passed")
                          <td class="alert alert-success">
                          @else
                            <td class="alert alert-danger">
                            @endif
                            {{$student->grades->where('semester', "$setting->semester")->where('grade', $student->grade)->first()->grades->where('subject_id', $sched->subject_id)->first()->remarks}}
                          </td>
                        @else
                          @if ($setting->encoding == 1)
                            <td>
                              <ul class="list-inline">
                                <li>
                                  <form class="form-inline" id="saveGrade-{{$student->student_id}}">
                                    <div class="form-group">
                                      <input type="number" name="grade" class="form-control">
                                      <input type="hidden" name="student_id" value="{{$student->student_id}}">
                                      <input type="hidden" name="sched_id" value="{{$sched->id}}">
                                    </div>
                                  </form>
                                </li>
                                <li>
                                  <button class="btn btn-success" id="modal-{{$student->student_id}}">Save</button>
                                </li>
                              </ul>
                            </td>
                            <td></td>
                          @else
                            <td></td>
                            <td></td>
                          @endif
                        @endif
                        <!-- modal -->
                        <div class="modal fade" id="confirmModal-{{$student->student_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLongTitle">Alert</h3>
                              </div>
                              <div class="modal-body">
                                <div class="alert alert-warning">
                                  <p>Once saved you cannot edit the student grade.</p>
                                  <p>Are you sure to save this grade?</p>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="save-{{$student->student_id}}">Save</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- END OVERVIEW -->
        </div>
      </div>
    </div>
    <!-- END MAIN CONTENT -->
  </div>
  <!-- END MAIN -->


@endsection

@section('js')
  <script type="text/javascript">

  $("[id^=modal-]").click(function(e) {
    var modalID = e.target.id.substring(e.target.id.indexOf("-") + 1);
    if($("#saveGrade-" + modalID).find('input[type=number]').val().length <= 0) {
      alert("Please Enter Grade");
      return false;
    } else if ($("#saveGrade-" + modalID).find('input[type=number]').val() < 60 || $("#saveGrade-" + modalID).find('input').val() > 100) {
      alert("Please Enter Valid Grade Between 60 And 100");
      return false;
    }
    $("#confirmModal-" + modalID).modal('toggle');
  });

  $("[id^=save-]").click(function(e) {
    var modalID = e.target.id.substring(e.target.id.indexOf("-") + 1);
    $("#confirmModal-" + modalID).modal('toggle');
    $("#saveGrade-" + modalID).submit();
  });

  $("[id^=saveGrade-]").submit(function(e){
    e.preventDefault();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/teacher/grading/encode',
      type: 'POST',
      data: $(this).serialize(),
      success:function(error) {
        if(error == "bad") {
          location.reload();
        }
        $("#tableHere").fadeOut();
        $("#tableHere").load("/teacher/grading/{{$strand->id}}/{{$sched->id}} #tableHere");
        $("#tableHere").fadeIn();
      }
    });
  })
  </script>
@endsection
