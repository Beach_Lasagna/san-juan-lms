@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              Submitted Answers<br>
            </p>
          </div>
          <div class="panel-body">
            <div class="row">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th><a href="/teacher/activity/{{$strand->id}}/{{$sched->id}}"><i class="lnr lnr-arrow-left-circle"></i></a>
                      <a href="#">{{$activity->title}}</a></th>
                      <th></th>
                      <th></th>
                      <th></th>

                    </tr>
                  </thead>
                  <thead>
                    <tr>
                      <th>Student</th>
                      <th>LRN</th>
                      <th>Response</th>
                      <th>File</th>

                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($activity->answers as $answer)
                      <tr>
                        <td>{{$answer->student->firstname}} {{$answer->student->middlename}} {{$answer->student->lastname}}</td>
                        <td>{{$answer->student->lrn}}</td>
                        <td>{{$answer->answer}}</td>
                        <td><a href="{{asset('storage/submitted-assignments/' . $answer->file)}}" download>{{$answer->file}}</a></td>
                      </tr>
                    @endforeach
                </table>



              </div>
            </div>
          </div>
          <!-- END OVERVIEW -->
        </div>
      </div>
    </div>
    <!-- END MAIN CONTENT -->
  </div>
  <!-- END MAIN -->
@endsection
