@extends('layouts.app')

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <div class="pull-right">
              <ul class="list-inline">
                @foreach ($sched as $s)
                  <li class="list-inline-item"><a class="btn btn-primary" href="{{ url('teacher/strand/' . $s->strand_id.'/' .$s->id ) }}"><h4>Student List</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="{{ url('teacher/grading/' . $s->strand_id. '/' .$s->id ) }}"><h4>Grades</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="{{ url('teacher/activity/' . $s->strand_id. '/' .$s->id ) }}"><h4>Activities</h4></a></li>
                  <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$s->strand_id}}/{{$s->id}}"><h4>Exams</h4></a></li>
                @endforeach
              </ul>
            </div>
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              @foreach ($sched as $day)
                {{$day->subject->subject_name}}
                <br>
                {{date('g:i A' ,strtotime($day->time_start))}} - {{date('g:i A' ,strtotime($day->time_end))}}
              @endforeach
            </p>
          </div>
          <div class="panel-body">
            <div class="row">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>LRN</th>
                    <th>Surname</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Gender</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($students as $student)
                    <tr>
                      <td>{{$student->lrn}}</td>
                      <td>{{$student->lastname}}</td>
                      <td>{{$student->firstname}}</td>
                      <td>{{$student->middlename}}</td>
                      <td>{{$student->gender}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- END OVERVIEW -->
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
