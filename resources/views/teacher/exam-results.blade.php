@extends('layouts.app')

@section('css')
@endsection

@section('content')
  <!-- MAIN -->
  <div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
          <div class="panel-heading">
            <div class="pull-right">
              <ul class="list-inline">
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/strand/{{$sched->strand_id}}/{{$sched->id}}"><h4>Student List</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/grading/{{$sched->strand_id}}/{{$sched->id}}"><h4>Grades</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/activity/{{$sched->strand_id}}/{{$sched->id}}"><h4>Activities</h4></a></li>
                <li class="list-inline-item"><a class="btn btn-primary" href="/teacher/exam/{{$sched->strand_id}}/{{$sched->id}}"><h4>Exams</h4></a></li>
              </ul>
            </div>
            <h3 class="panel-title">Grade {{$strand->grade}} {{$strand->strand}}</h3>
            <p class="panel-subtitle">
              {{$sched->subject->subject_name}} <br>
              {{date('g:i A' ,strtotime($sched->time_start))}} - {{date('g:i A' ,strtotime($sched->time_end))}}
            </p>
            <br>
            <h3 class="panel-title">{{$exam->title}} Results</h3>
            <p class="panel-subtitle">{{$exam->description}}</p>
            <br><br>
          </div>
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <th>Student</th>
                <th>Score</th>
                <th>Remarks</th>
                <th>Date Submitted</th>
              </thead>
              <tbody>
                @foreach ($examHeads as $answer)
                  <tr>
                    <td>{{$answer->student->lastname}}, {{$answer->student->firstname}} {{$answer->student->middlename}}</td>
                    <td>{{$answer->score}}</td>
                    @if ($answer->remarks == 'Passed')
                      <td class="alert alert-success">{{$answer->remarks}}</td>
                    @else
                      <td class="alert alert-danger">{{$answer->remarks}}</td>
                    @endif
                    <td>{{date('M j Y g:i A' ,strtotime($answer->updated_at))}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- END OVERVIEW -->
        </div>
      </div>
    </div>
    <!-- END MAIN CONTENT -->
  </div>
  <!-- END MAIN -->

  <!-- Modals -->
  <div class="modal fade" id="addQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add Question</h3>
        </div>
        <div class="modal-body">
          <form action="/teacher/exam/questions/store" method="POST" autocomplete="off" enctype="multipart/form-data" id="storeQuestion">
            @csrf
            <input type="hidden" name="exam_id" value="{{$exam->id}}"/>
            <div class="form-group">
              <label for="question">Question : </label>
              <textarea name="question"  class="form-control" style="resize: none;" required></textarea>
            </div>
            <div class="form-group">
              <label for="a">Choice A : </label>
              <textarea name="a"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="b">Choice B : </label>
              <textarea name="b"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="c">Choice C : </label>
              <textarea name="c"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="d">Choice D : </label>
              <textarea name="d"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="answer">Answer : </label>
              <select name="answer" class="form-control">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
              </select>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit Activity Modal -->
  <div class="modal fade" id="editQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Edit Question</h3>
        </div>
        <div class="modal-body">
          <form action="/teacher/exam/questions/edit/store" method="POST" autocomplete="off" enctype="multipart/form-data" id="storeEditedQuestion">
            @csrf
            <input type="hidden" name="question_id" id="question_id"/>
            <div class="form-group">
              <label for="question">Question : </label>
              <textarea name="question" id="edit_question"  class="form-control" style="resize: none;" required></textarea>
            </div>
            <div class="form-group">
              <label for="a">Choice A : </label>
              <textarea name="a" id="edit_a"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="b">Choice B : </label>
              <textarea name="b" id="edit_b"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="c">Choice C : </label>
              <textarea name="c" id="edit_c"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="d">Choice D : </label>
              <textarea name="d" id="edit_d"  class="form-control" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <label for="answer">Answer : </label>
              <select name="answer" id="edit_answer" class="form-control">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
              </select>
            </div>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- delete Activity Modal -->
  <div class="modal fade" id="deleteQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Delete Question</h3>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
          <form action="/teacher/exam/questions/delete" method="POST" autocomplete="off" enctype="multipart/form-data" id="deleteQuestionFom">
            @csrf
            <input type="hidden" name="question_id_del" id="question_id_del"/>
            <div class="modal-footer">
              <div class="row">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modals -->
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function() {
    $('#addQuestion').click(function () {
      $('#addQuestionModal').modal('show');
    });

    $("[id^=deleteQuestion-]").click(function (e) {
      $("#question_id_del").val(e.target.id.substring(e.target.id.indexOf("-") + 1));
    });
  });

  </script>
  <script type="text/javascript">
  $(".editQuestion").click(function(e) {
    // var question_id = e.target.id.substring(e.target.id.indexOf("-") + 1);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/teacher/exam/questions/edit',
      type: 'POST',
      data: {
        questionID: $(this).val(),
      },
      success:function(question) {
        $('#question_id').val(question.id);
        $('#edit_question').val(question.question);
        $('#edit_a').val(question.A);
        $('#edit_b').val(question.B);
        $('#edit_c').val(question.C);
        $('#edit_d').val(question.D);
        switch (question.answer) {
          case 'A':
          $("select option[value='A']").attr("selected","selected");
          break;
          case 'B':
          $("select option[value='B']").attr("selected","selected");
          break;
          case 'C':
          $("select option[value='C']").attr("selected","selected");
          break;
          case 'D':
          $("select option[value='D']").attr("selected","selected");
          break;
        }
      }
    });
  });
  </script>
  <script type="text/javascript">
  // $("#storeQuestion").submit(function(e) {
  //   e.preventDefault();
  //   $.ajax({
  //     url: '/teacher/exam/questions/store',
  //     type: 'POST',
  //     data: $("#storeQuestion").serialize(),
  //     success:function() {
  //       $('#addQuestionModal').modal('hide');
  //       $('#storeQuestion').trigger("reset");
  //       $("#tableHere").fadeOut();
  //       $("#tableHere").load("/teacher/exam/questions/table/{{$exam->id}}", function() {
  //         $("#tableHere").fadeIn();
  //       });
  //     }
  //   });
  // });
  </script>
@endsection
