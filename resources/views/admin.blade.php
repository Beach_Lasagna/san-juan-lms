@extends('layouts.app')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">

                <div class="panel panel-headline">
                        <div class="panel-heading">
                            <h3 class="panel-title">Summary</h3>
                            <p class="panel-subtitle">Period: October 1, 2018 - Oct 5, 2018</p>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><img src="{{ asset('images/icons/student-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" ></span>
                                        <p>
                                            <span class="number">{{ $stud }}</span>
                                            <span class="title">Students</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><img src="{{ asset('images/icons/teacher-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" ></span>
                                        <p>
                                            <span class="number">{{ $teach }}</span>
                                            <span class="title">Faculty</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><img src="{{ asset('images/icons/subject-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" ></span>
                                        <p>
                                            <span class="number">{{ $subj }}</span>
                                            <span class="title">Subjects</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><img src="{{ asset('images/icons/class-icon.png') }}" style="height:35px; width: 35px; line-height: 35px;" ></span>
                                        <p>
                                            <span class="number">{{ $strand }}</span>
                                            <span class="title">Strands</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <div class="panel-body">
                @can("isSysAdmin")


                @endcan
                @can("isAdmin")
                @endcan
                @can("isFaculty")
                grade ang student,
                @endcan
                @can("isStudent")
                view grade, aseasd
                @endcan
                </div>
                <br><br>

        </div>
    </div>
</div>

@endsection
