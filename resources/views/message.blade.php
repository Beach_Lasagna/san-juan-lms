<html>
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Ajax Example</title>


  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
  </script>

  <script>
  jQuery(document).ready(function(){
    $('#ajaxSubmit').click(function() {
      $.ajax({
         headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url: '/getmsg',
         dataType : 'json',
         type: 'POST',
         data: {},
         contentType: false,
         processData: false,
         success:function(response) {
              alert(response.success);
         }
    });
    })
  })
  </script>
</head>

<body>
  <div id = 'msg'>This message will be replaced using Ajax.
    Click the button to replace the message.</div>
    <button id = 'ajaxSubmit' class="btn btn-default">Replace Message</button>
  </body>

  </html>
