@extends('layouts.app')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">

                <div class="panel panel-headline">
                    <div class="panel-heading">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br>
                    </div>
                    Welcome {{ Auth::user()->profile }} {{ Auth::user()->usertype }} {{ Auth::user()->firstname }}
                    <br>
                </div>
                <div class="panel-body">
                @can("isSysAdmin")


                @endcan
                @can("isAdmin")
                dito add ka ng sched sa prof, add ng prof
                @include("admin")
                @endcan
                @can("isFaculty")
                grade ang student,
                @endcan
                @can("isStudent")
                view grade, aseasd
                @endcan
                </div>
                <br><br>

</div>
    </div>
    </div>

@endsection
