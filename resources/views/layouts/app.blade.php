<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>San Juan HS LMS</title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<!-- Ionicons -->
	<!-- <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}"> -->
	<!-- Theme style -->
	<!-- <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}" -->
	<!-- iCheck -->
	<!-- <link rel="stylesheet" href="{{ asset('css/blue.css') }}"> -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<!-- <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}"> -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}"> -->
	<!-- link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->
	<link rel="stylesheet" href="{{ asset('css/chartist-custom.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}"> -->
	<link rel="stylesheet" href="{{ asset('css/icon-font.min.css') }}">
	<link rel="stylesheet" href="{{asset('css/fullcalendar.css')}}" />
	<!-- icon -->
	<link rel="icon" type="image/png" sizes="96x96" href="{{asset('images/icons/SJNHS.png')}}">
	<link rel='stylesheet' href="{{asset('datetime-picker/build/css/bootstrap-glyphicons.css')}}" />
	@yield('css')
</head>
<body class="hold-transition ">
	<div id="wrapper">
		@auth
			@include('navbars.navbar')
			@can ('isAdmin')
				@include('navbars.admin-sidebar')
			@endcan
			@can ('isTeacher')
				@include('navbars.teacher-sidebar')
			@endcan
			@can ('isStudent')
				@include('navbars.student-sidebar')
			@endcan
		@endauth
			@yield('content')
	</div>
	@include('layouts.footer')

	<div id="processing" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title">Processing</h4>
	      </div>
	      <div class="modal-body">
	        <p>Please wait...</p>
	      </div>
	    </div>

	  </div>
	</div>
</body>
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script>
$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' /* optional */
	});
});
</script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-3.3.1.js') }}"></script>
<!-- <script src="{{ asset('js/bootstrap.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/chartist.min.js') }}"></script>
<script src="{{ asset('js/klorofil-common.js') }}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/fullcalendar.min.js')}}"></script>

<script>
$(function() {
	var data, options;

	// headline charts
	data = {
		labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
		series: [
			[23, 29, 24, 40, 25, 24, 35],
			[14, 25, 18, 34, 29, 38, 44],
		]
	};

	options = {
		height: 300,
		showArea: true,
		showLine: false,
		showPoint: false,
		fullWidth: true,
		axisX: {
			showGrid: false
		},
		lineSmooth: false,
	};
	new Chartist.Line('#headline-chart', data, options);
	// visits trend charts
	data = {
		labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		series: [{
			name: 'series-real',
			data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
		}, {
			name: 'series-projection',
			data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
		}]
	};
	options = {
		fullWidth: true,
		lineSmooth: false,
		height: "270px",
		low: 0,
		high: 'auto',
		series: {
			'series-projection': {
				showArea: true,
				showPoint: false,
				showLine: false
			},
		},
		axisX: {
			showGrid: false,
		},
		axisY: {
			showGrid: false,
			onlyInteger: true,
			offset: 0,
		},
		chartPadding: {
			left: 20,
			right: 20
		}
	};
	new Chartist.Line('#visits-trends-chart', data, options);
	// visits chart
	data = {
		labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
		series: [
			[6384, 6342, 5437, 2764, 3958, 5068, 7654]
		]
	};
	options = {
		height: 300,
		axisX: {
			showGrid: false
		},
	};
	new Chartist.Bar('#visits-chart', data, options);
	// real-time pie chart
	var sysLoad = $('#system-load').easyPieChart({
		size: 130,
		barColor: function(percent) {
			return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1.1 - percent / 100)) + ", 0)";
		},
		trackColor: 'rgba(245, 245, 245, 0.8)',
		scaleColor: false,
		lineWidth: 5,
		lineCap: "square",
		animate: 800
	});
	var updateInterval = 3000; // in milliseconds
	setInterval(function() {
		var randomVal;
		randomVal = getRandomInt(0, 100);
		sysLoad.data('easyPieChart').update(randomVal);
		sysLoad.find('.percent').text(randomVal);
	}, updateInterval);
	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
});
</script>
<script >
$(document).ready(function() {
 $(window).keydown(function(event){
     if((event.keyCode == 13) && ($(event.target)[0]!=$("textarea")[0])) {
         event.preventDefault();
         return false;
     }
 });
});
</script>
@yield('js')
</html>
