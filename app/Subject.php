<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = ['subject_name'];

      public function student()
      {
          return $this->belongsTo('App\Student');
      }
      public function teachers()
      {
          return $this->hasMany('App\TeacherProfile');
      }
      public function grade()
      {
          return $this->hasMany('App\Grade');
      }

      public function schedules()
      {
          return $this->hasMany('App\Schedule', 'subject_id');
      }
}
