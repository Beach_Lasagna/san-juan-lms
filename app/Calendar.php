<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'events';

    public function teacher()
    {
      return $this->belongsTo('App\TeacherProfile', 'teacher_id');
    }

    public function strand()
    {
      return $this->belongsTo('App\Strand', 'strand_id');
    }
}
