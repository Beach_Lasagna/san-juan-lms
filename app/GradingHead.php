<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradingHead extends Model
{
    protected $table = 'gradingheads';
    protected $fillable = [
        'grade', 'semester', 'student_id', 'strand_id', 'school_year', 'gpa', 'strand',
    ];


    public function student() {
      return $this->belongsTo('App\Student', 'student_id');
    }

    public function strand() {
      return $this->belongsTo('App\Strand', 'strand_id');
    }

    public function grades() {
      return $this->hasMany('App\Grade', 'head_id');
    }
}
