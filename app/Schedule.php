<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    // protected $dates = ['time_start',
    //     'time_end',
    // ];
		protected $fillable = ['teacher_profile_id', 'subject_id', 'strand_id', 'day', 'time_start', 'time_end',
    ];

		public function myTeacher(){
    	return $this->belongsTo('App\TeacherProfile', 'teacher_profile_id', 'teacher_id');
    }

    public function subject(){
        return $this->belongsTo('App\Subject', 'subject_id');
    }

		public function strand(){
        return $this->belongsTo('App\Strand', 'strand_id');
    }

		public function myActivities() {
			return $this->hasMany('App\Activity', 'sched_id');
		}

		public function exams() {
			return $this->hasMany('App\Exam', 'sched_id');
		}
}
