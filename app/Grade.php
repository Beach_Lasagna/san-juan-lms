<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
  public function head()
  {
      return $this->belongsTo('App\GradingHead', 'head_id');
  }
  public function subject()
  {
      return $this->belongsTo('App\Subject');
  }

  public function teacher()
  {
      return $this->belongsTo('App\TeacherProfile', 'teacher_id');
  }
}
