<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model
{
  public function exam() {
    return $this->belongsTo('App\Exam', 'exam_id');
  }

  public function tempQuestions() {
    return $this->hasMany('App\TemporaryAnswer', 'question_id');
  }
}
