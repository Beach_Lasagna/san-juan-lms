<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Strand extends Model
{
    protected $fillable = ['grade', 'strand',];

    public function students(){
    	return $this->hasMany('App\Student', 'strand');
    }

    public function schedules(){
    	return $this->hasMany('App\Schedule', 'strand_id');
    }

    public function events()
    {
      return $this->hasMany('App\Calendar', 'strand_id');
    }

    public function grading() {
      return $this->hasMany('app\GradingHead', 'strand_id');
    }
}
