<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'student_id';

    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'gender', 'lrn', 'grade', 'strand',
    ];

    public function myStrand()
    {
        return $this->belongsTo('App\Strand', 'strand');
    }

    public function grades(){
        return $this->hasMany('App\GradingHead', 'student_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function answers(){
        return $this->hasMany('App\SubmittedActivity', 'student_id');
    }

    public function answeredExams() {
      return $this->hasMany('App\AnsweredExam', 'student_id');
    }
}
