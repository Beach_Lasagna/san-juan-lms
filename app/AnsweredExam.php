<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnsweredExam extends Model
{
    protected $table = 'exam_answers';

    public function student() {
      return $this->belongsTo('App\Student', 'student_id');
    }

    public function details() {
      return $this->hasMany('App\ExamAnswers', 'exam_answers_id');
    }

    public function exam() {
      return $this->belongsTo('App\Exam', 'exam_id');
    }
}
