<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherProfile extends Model
{
  protected $primaryKey = 'teacher_id';
  protected $fillable = ['firstname',
  'middlename',
  'lastname',
  'position',
  'address',
  'age',
  'birthmonth',
  'birthday',
  'birthyear',
  'school_graduated',
  'year_graduated',
  'educational_attainment',
  'subjects',
  'contact',
  'email',];

  public function teachersched()
  {
    return $this->hasMany('App\Schedule', 'teacher_profile_id');
  }
  public function user()
  {
    return $this->belongsTo('App\User');
  }
  public function activities()
  {
    return $this->hasMany('App\Activity', 'teacher_id');
  }

  public function events()
  {
    return $this->hasMany('App\Calendar', 'teacher_id');
  }

  public function grades()
  {
    return $this->hasMany('App\Grade', 'teacher_id');
  }
}
