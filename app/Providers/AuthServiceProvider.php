<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {

        $this->registerPolicies($gate);
        $gate->define('isSysAdmin', function($user){
            return $user->usertype == 'System Administrator';
        });

        $gate->define('isAdmin', function($user){
            return $user->usertype == 'Admin';
        });
        $gate->define('isTeacher', function($user){
            return $user->usertype == 'Teacher';
        });
        $gate->define('isStudent', function($user){
            return $user->usertype == 'Student';
        });

        //
    }



}
