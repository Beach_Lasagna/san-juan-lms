<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\TeacherProfile;
use App\Student;
use App\Subject;
use App\Schedule;
use App\Settings;
use App\Strand;
use App\GradingHead;
use Session;

class AddData extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function addData(Request $request){
    // $pass = $request->input('password');
    // $cryptthis = Hash::make($pass);
    // $data = array('firstname'=> $request->input('firstname'),
    //      'middlename'=> $request->input('middlename'),
    //      'lastname'=> $request->input('lastname'),
    //      'email'=> $request->input('email'),
    //      'password'=> $cryptthis,
    //      'usertype'=> $request->input('usertype'));
    // DB::table('users')->insert($data);
    // Student::create($request->all());
    // return back();
  }
  public function addTeacher(Request $request){
    // TeacherProfile::create($request->all());
    // return back();
    // return 123;
    $user = new User;
    $user->firstname = $request->firstname;
    $user->middlename = $request->middlename;
    $user->lastname = $request->lastname;
    $user->usertype = 'Teacher';
    $user->email = $request->email;
    $user->password = Hash::make('123456');
    $user->save();
    $user = User::orderBy('created_at', 'des')->take(1)->get();
    foreach ($user as $u) {
      $last_id = $u->id;
    }
    $student = new TeacherProfile;
    $student->firstname = $request->firstname;
    $student->middlename = $request->middlename;
    $student->lastname = $request->lastname;
    $student->position = $request->position;
    $student->address = $request->address;
    $student->age = $request->age;
    $student->birthmonth = $request->birthmonth;
    $student->birthday = $request->birthday;
    $student->birthyear = $request->birthyear;
    $student->school_graduated = $request->school_graduated;
    $student->year_graduated = $request->year_graduated;
    $student->educational_attainment = $request->educational_attainment;
    $student->subjects = $request->subjects;
    $student->contact = $request->contact;
    $student->email = $request->email;
    $student->user_id = $last_id;
    $student->save();

    return back();
  }
  public function addSubject(Request $request){
    Subject::create($request->all());
    return back();
  }
  public function addStudent(Request $request){
    $user = new User;
    $user->firstname = $request->firstname;
    $user->middlename = $request->middlename;
    $user->lastname = $request->lastname;
    $user->usertype = 'Student';
    $user->email = $request->email;
    $user->password = Hash::make('123456');
    $user->save();
    $user = User::orderBy('created_at', 'des')->take(1)->get();
    foreach ($user as $u) {
      $last_id = $u->id;
    }
    $student = new Student;
    $student->firstname = $request->firstname;
    $student->middlename = $request->middlename;
    $student->lastname = $request->lastname;
    $student->gender = $request->gender;
    $student->lrn = $request->lrn;
    $student->strand = $request->strand;
    $student->grade = $request->grade;
    $student->user_id = $last_id;
    $student->save();

    $setting = Settings::all()->first();
    $students = Student::orderBy('created_at', 'des')->take(1)->get();
    foreach ($students as $u) {
      $student = $u;
      $last_id = $u->student_id;
    }

    $gradingHead = new GradingHead;
    $gradingHead->grade = $student->grade;
    $gradingHead->semester = $setting->semester;
    $gradingHead->student_id = $last_id;
    $gradingHead->strand_id = $student->strand;
    $gradingHead->school_year = $setting->school_year;
    $gradingHead->save();

    return redirect('/admin/student');
    //return $request;
  }
  public function addSched(Request $request){
    $sched = new Schedule;
    $sched->teacher_profile_id = $request->teacher_profile_id;
    $sched->subject_id = $request->subject_id;
    $sched->strand_id = $request->strand_id;
    $sched->day = $request->day;
    $sched->time_start = date("H:i:s", strtotime($request->time_start));
    $sched->time_end = date("H:i:s", strtotime($request->time_end));
    $sched->save();
    return back();
  }
  public function addStrand(Request $request){
    Strand::create($request->all());
    return back();
  }
  public function deleteTeacher(Request $request){
   TeacherProfile::find($request->teacher_id)->delete();
   User::find($request->user_id)->delete();
    return back();
  }
  public function deleteSubject(Request $request){
    Subject::destroy($request->id);
    return back();
  }
  public function deleteStudent(Request $request){

   Student::find($request->student_id)->delete();
   User::find($request->user_id)->delete();
   return back();
  }
  public function deleteSched(Request $request){
     Schedule::destroy($request->id);
    return back();
  }
  public function deleteStrand(Request $request){
    Strand::find($request->id)->delete();
    return back();
  }

  public function editTeacher(Request $request){
    $user =  User::find($request->user_id);
    $user->firstname = $request->firstname;
    $user->middlename = $request->middlename;
    $user->lastname = $request->lastname;
    $user->email = $request->email;
    $user->save();

    $student = TeacherProfile::find($request->teacher_id);
    $student->firstname = $request->firstname;
    $student->middlename = $request->middlename;
    $student->lastname = $request->lastname;
    $student->position = $request->position;
    $student->address = $request->address;
    $student->age = $request->age;
    $student->birthmonth = $request->birthmonth;
    $student->birthday = $request->birthday;
    $student->birthyear = $request->birthyear;
    $student->school_graduated = $request->school_graduated;
    $student->year_graduated = $request->year_graduated;
    $student->educational_attainment = $request->educational_attainment;
    $student->subjects = $request->subjects;
    $student->contact = $request->contact;
    $student->email = $request->email;
    $student->save();

    return back();
  }
  public function editSubject(Request $request){
    $subj = Subject::find($request->id);
    $subj->subject_name = $request->subject_name;
    $subj->save();
    return back();
  }
  public function editSched(Request $request){

   $sched = Schedule::find($request->id);
   $sched->teacher_profile_id = $request->teacher_profile_id;
   $sched->subject_id = $request->subject_id;
   $sched->strand_id = $request->strand_id;
   $sched->day = $request->day;
   $sched->grade = $request->grade;
   $sched->semester = $request->semester;
   $sched->time_start = $request->time_start;
   $sched->time_end = $request->time_end;
   $sched->save();
   return back();
  }
  public function editStudent(Request $request){
   $stud = Student::find($request->id);
   $stud->firstname = $request->firstname;
   $stud->middlename = $request->middlename;
   $stud->lastname = $request->lastname;
   $stud->gender = $request->gender;
   $stud->lrn = $request->lrn;
   $stud->grade = $request->grade;
   $stud->strand = $request->strand;
   $stud->save();
   return back();
  }
  public function editSettings(Request $request){
   $set = Settings::find($request->id)->first();
   $set->enrollment= $request->enrollment;
   $set->encoding= $request->encoding;
   $set->semester= $request->semester;
   $set->save();
   return back();
  }
  public function editStrand(Request $request){
    $strand = Strand::find($request->id);
    $strand->grade = $request->grade;
    $strand->strand = $request->strand;
    $strand->save();
    return back();
  }









}
