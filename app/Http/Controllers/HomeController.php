<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\TeacherProfile;
use App\Schedule;
use App\Settings;
use App\Student;
use App\Subject;
use App\Strand;
use App\User;
use Auth;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  private function settingsInit() {
    $setting = Settings::all();
    if(count($setting) <= 0)
    {
      $setting = new Settings;
      $setting->school_year_start = date("Y", strtotime(date("r")));
      $setting->school_year_end = date("Y", strtotime(date("r"))) + 1;
      $setting->school_year = $setting->school_year_start . " - " . $setting->school_year_end;
      $setting->save();
    }
  }

  public function endSemester() {
    $setting = Settings::all()->first();
    switch ($setting->semester) {
      case 1:
      $setting->semester = 2;
      break;
      case 2:
      $setting->semester = 1;
      $setting->school_year_start++;
      $setting->school_year_end++;
      $setting->school_year = $setting->school_year_start . " - ". $setting->school_year_end;
      $students = Student::all()->where('grade', 11);
      foreach ($students as $student) {
        $student->grade = 12;
        $student->save();
      }
      break;
    }
    $setting->save();
    $students = Student::all()->where('enrolled', '1');
    foreach ($students as $student) {
      $student->enrolled = 0;
      $student->save();
    }
    $schedules = Schedule::all();
    foreach ($schedules as $schedule) {
      $schedule->teacher_profile_id = null;
      $schedule->save();
    }
    return redirect('/admin/settings')->with('success', 'Semester Successfully Ended.');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $stud = Student::all();
    $teach = TeacherProfile::all();
    $subj = Subject::all();
    // $schedule =Schedule::all()->orderBy('time_start', 'asc')->get();
    $sched = Schedule::all();
    //return view("home", compact("stud", "teach", "subj", "sched"));
    switch (auth()->user()->usertype) {
      case 'Admin':
      return redirect('/admin');
      break;
      case 'Teacher':
      return redirect('/teacher');
      break;
      case 'Student':
      return redirect('/student');
      break;
    }
  }

  public function showsched()
  {
    $teach = TeacherProfile::all();
    $sched = Schedule::all();
    $subj = Subject::all();
    $strand = Strand::all();
    return view("schedule", compact("teach", "subj", "sched", "strand"));
  }
  public function showstud()
  {
    $strand = Strand::all();
    $stud = Student::all();
    return view("student", compact("stud", "strand"));
  }
  public function showstrand()
  {
    $strand = Strand::all();
    return view("strand", compact("strand"));
  }
  public function showsettings()
  {
    $this->settingsInit();
    $setting = Settings::all();
    return view("settings", compact("setting"));
  }
  public function showteach()
  {
    $teach = TeacherProfile::all();
    return view("teacher", compact("teach"));
  }
  public function showsubj()
  {
    $subj = Subject::all();
    // $strand = Strand::all();
    return view("subject", compact("subj"));
  }
  public function admin()
  {
    $stud = Student::count();
    $teach = TeacherProfile::count();
    $subj = Subject::count();
    $strand = Strand::count() * 2;

    return view("admin", compact("stud", "teach", "subj", "strand"));
  }
}
