<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\TeacherProfile as Teacher;
use App\Calendar as Event;
use App\User;
use App\Schedule;
use App\Strand;
use App\Student;
use App\Grade;
use App\Activity;
use App\Exam;
use App\ExamQuestion;
use App\Settings;
use App\GradingHead;
use Auth;

class TeacherController extends Controller
{
  private function checkUser() {
    if(auth()->user()->usertype != "Teacher") {
      abort(403, 'Unauthorized action.');
    }
  }

  private function getTeacher() {
    $user = User::find(auth()->user()->id);
    $teachers = $user->teacherProfile;
    foreach ($teachers as $t) {
      $teacher = $t;
    }
    return $teacher;
  }

  private function getSchedule($schedID, $scheds) {
    $teacher = $this->getTeacher();
    $sched_var = $scheds->where('id', $schedID);
    foreach ($sched_var as $s) {
      $sched = $s;
    }
    return $sched;
  }

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $teacher = $user->teacherProfile;
    foreach ($teacher as $t) {
      $sched = $t->teachersched;
      $monday = $t->teachersched()->where('day', 'Monday')->orderBy('time_start')->get();
      $tuesday = $t->teachersched()->where('day', 'Tuesday')->orderBy('time_start')->get();
      $wednesday = $t->teachersched()->where('day', 'Wednesday')->orderBy('time_start')->get();
      $thursday = $t->teachersched()->where('day', 'Thursday')->orderBy('time_start')->get();
      $friday = $t->teachersched()->where('day', 'Friday')->orderBy('time_start')->get();
    }
    return view('teacher.index')->with('teacher', $teacher)
    ->with('scheds', $sched)
    ->with('monday', $monday)
    ->with('tuesday', $tuesday)
    ->with('wednesday', $wednesday)
    ->with('thursday', $thursday)
    ->with('friday', $friday);
  }

  public function strand($strand_id, $sched_id)
  {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $strand = Strand::find($strand_id);
    $sched = Schedule::where('id', $sched_id)->get();
    $students = $strand->students()->orderBy('lastname')->get();
    $teacher = $user->teacherProfile;
    foreach ($teacher as $t) {
      $scheds = $t->teachersched;
    }
    return view('teacher.strand')->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('students', $students)
    ->with('strand', $strand);
  }

  public function grading($strand_id, $sched_id)
  {
    $this->checkUser();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();
    $setting = Settings::all()->first();
    $grades = Grade::all();
    return view('teacher.grading')->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('students', $students)
    ->with('strand', $strand)
    ->with('setting', $setting)
    ->with('grades', $grades);
  }

  public function getStudent(Request $request) {
    $student = Student::find($request->studentID);
    return response()->json($student);
  }

  public function encode(Request $request) {
    $setting = Settings::all()->first();
    $teacher = $this->getTeacher();
    $sched =  Schedule::find($request->sched_id);
    $student = Student::find($request->student_id);
    $gradingHead = GradingHead::where('student_id', $student->student_id)->where('grade', $student->grade)->first();
    if($setting->encoding == 0) {
      return response()->json('bad');
    }
    $grade = new Grade;
    $grade->subject_id = $sched->subject_id;
    $grade->teacher_id = $teacher->teacher_id;
    $grade->student_id = $student->student_id;
    $grade->grade = $request->grade;
    if($grade->grade >= 75) {
      $grade->remarks = "Passed";
    } else {
      $grade->remarks = "Failed";
    }
    $grade->head_id = $gradingHead->id;
    $grade->save();
    return response()->json('good');
  }

  public function activity($strand_id, $sched_id)
  {
    $this->checkUser();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();
    return view('teacher.activity')->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('students', $students)
    ->with('strand', $strand)
    ->with('activities', $teacher->activities()->where('sched_id', $sched_id)->orderBy('created_at', 'dsc')->get());
  }

  public function activityStore(Request $request, $sched_id) {
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();

    $this->validate($request, [
      'title' => 'required|string',
      'description' => 'required|string',
      'deadline' => 'required|date|after:' . date("r"),
      'file' => 'mimes:doc,docx|nullable|max:4999',
    ]);

    //file upload
    if($request->hasFile('file')) {
      //get filename w/ ext
      $filenameWithExt = $request->file('file')->getClientOriginalName();
      //get just file name
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
      //get just ext
      $ext = $request->file('file')->getClientOriginalExtension();
      //file name to store
      $fileNameToStore = $filename . "_" . time() . "." . $ext;
      //upload image
      $path = $request->file('file')->storeAs('public/assignments', $fileNameToStore);
    } else {
      $fileNameToStore = '';
    }

    $activity = new Activity;
    $activity->title = $request->input('title');
    $activity->description = $request->input('description');
    $activity->type = $request->input('type');
    $activity->file = $fileNameToStore;
    $activity->strand_id = $strand->id;
    $activity->teacher_id = $teacher->teacher_id;
    $activity->sched_id = $sched->id;
    $activity->deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
    $activity->save();
    return redirect('teacher/activity/' . $strand->id . '/' . $sched->id)
    ->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('students', $students)
    ->with('strand', $strand);
  }

  public function activityEdit(Request $request, $sched_id) {
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $this->validate($request, [
      'title' => 'required|string',
      'description' => 'required|string',
      'deadline' => 'required|date|after:' . date("r"),
      'file' => 'mimes:doc,docx|nullable|max:4999',
    ]);

    $activity = Activity::find($request->input('activity_id'));

    //file upload
    if($request->hasFile('file')) {
      Storage::delete('public/assignments/'. $activity->file);
      //get filename w/ ext
      $filenameWithExt = $request->file('file')->getClientOriginalName();
      //get just file name
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
      //get just ext
      $ext = $request->file('file')->getClientOriginalExtension();
      //file name to store
      $fileNameToStore = $filename . "_" . time() . "." . $ext;
      //upload image
      $path = $request->file('file')->storeAs('public/assignments', $fileNameToStore);
    } else {
      $fileNameToStore = $activity->file;
    }

    $activity->title = $request->input('title');
    $activity->description = $request->input('description');
    $activity->type = $request->input('type');
    $activity->deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
    $activity->file = $fileNameToStore;
    $activity->save();
    return redirect('teacher/activity/' . $strand->id . '/' . $sched->id);
  }

  public function activityDelete($id, $sched_id) {
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $activity = Activity::find($id);
    if($activity->file != '') {
      Storage::delete('public/assignments/'. $activity->file);
    }
    $activity->delete();
    return redirect('teacher/activity/' . $strand->id . '/' . $sched->id);
  }

  public function calendar() {
    $this->checkUser();
    $strands = Strand::all();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $events = $teacher->events;
    $data = array();
    foreach($events as $row)
    {
      $data[] = array(
        'id'   => $row["id"],
        'title'   => $row["title"],
        'start'   => $row["start_date"],
        'end'   => $row["end_date"],
        'description'   => $row["description"],
        'strand'   => $row["strand_id"],
      );
    }
    return view('teacher.calendar')
    ->with('events', json_encode($data))
    ->with('scheds', $scheds)
    ->with('strands', $strands);
  }

  public function calendarAdd(Request $request) {
    $this->validate($request, [
      'title' => 'required|string|max:255',
      'start_date' => 'required|date',
      'end_date' => 'required|date',
      'description' => 'nullable|string',
    ]);
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $event = new Event;
    $event->title = $request->title;
    $event->start_date = $request->start_date;
    $event->end_date = $request->end_date;
    $event->description = $request->description;
    $event->strand_id = $request->strand;
    $event->teacher_id = $teacher->teacher_id;
    $event->save();
    return redirect('/teacher/calendar');
  }

  public function calendarEdit(Request $request) {
    $this->validate($request, [
      'title' => 'required|string|max:255',
      'start_date' => 'required|date',
      'end_date' => 'required|date',
      'description' => 'nullable|string',
    ]);
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $event = Event::find($request->event_id);
    $event->title = $request->title;
    $event->start_date = $request->start_date;
    $event->end_date = $request->end_date;
    $event->description = $request->description;
    $event->strand_id = $request->strand;
    $event->teacher_id = $teacher->teacher_id;
    $event->save();
    return redirect('/teacher/calendar');
  }

  public function calendarDelete(Request $request) {
    $event = Event::find($request->delete_id);
    $event->delete();
    return back();
  }

  public function getAct(Request $request) {
    $activity = Activity::find($request->actID);
    return response()->json($activity);
  }

  public function viewAct($act_id, $sched_id) {
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $activity = Activity::find($act_id);
    return view('teacher.view-activity')
    ->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('strand', $strand)
    ->with('activity', $activity);
  }

  public function exam($strand_id, $sched_id)
  {
    $this->checkUser();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();
    $exams = Exam::where('sched_id', $sched_id)->orderBy('created_at', 'des')->get();
    return view('teacher.exam')->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('students', $students)
    ->with('strand', $strand)
    ->with('exams', $exams);
  }

  public function examStore(Request $request, $sched_id) {
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();

    $this->validate($request, [
      'title' => 'required|string',
      'description' => 'required|string',
      'deadline' => 'required|date|after:' . date("r"),
    ]);

    $exam = new Exam;
    $exam->title = $request->input('title');
    $exam->description = $request->input('description');
    $exam->teacher_id = $teacher->teacher_id;
    $exam->sched_id = $sched->id;
    $exam->deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
    $exam->save();
    return back();
  }

  public function getExam(Request $request) {
    $exam = Exam::find($request->examID);
    return response()->json($exam);
  }

  public function editExam(Request $request) {
    $this->validate($request, [
      'title' => 'required|string',
      'description' => 'required|string',
      'deadline' => 'required|date|after:' . date("r"),
    ]);
    $exam = Exam::find($request->exam_id);
    $exam->title = $request->title;
    $exam->description = $request->description;
    $exam->deadline = date("Y-m-d H:i:s", strtotime($request->deadline));
    $exam->save();
    return back();
  }

  public function deleteExam($exam_id) {
    $exam = Exam::find($exam_id);
    $questions = ExamQuestion::where('exam_id', $exam->id)->get();
    if(count($questions) > 0) {
      foreach ($questions as $q) {
        $q->delete();
      }
    }
    $exam->delete();
    return back();
  }

  public function examQuestion($strand_id, $sched_id, $exam_id)
  {
    $this->checkUser();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $students = $strand->students()->orderBy('lastname')->get();
    $exam = Exam::find($exam_id);
    $num = 0;
    return view('teacher.exam-questions')->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('students', $students)
    ->with('strand', $strand)
    ->with('exam', $exam)
    ->with('questions', $exam->questions)
    ->with('num', $num);
  }

  public function storeQuestion(Request $request) {
    $this->validate($request, [
      'question' => 'required|string',
      'a' => 'required|string',
      'b' => 'required|string',
      'c' => 'required|string',
      'd' => 'required|string',
      'answer' => 'required|string',
    ]);

    $question = new ExamQuestion;
    $question->question = $request->question;
    $question->exam_id = $request->exam_id;
    $question->a = $request->a;
    $question->b = $request->b;
    $question->c = $request->c;
    $question->d = $request->d;
    $question->answer = $request->answer;
    $question->save();
    return back();
  }

  public function editQuestion(Request $request) {
    $question = ExamQuestion::find($request->questionID);
    return response()->json($question);
  }

  public function storeEdittedQuestion(Request $request) {
    $this->validate($request, [
      'question' => 'required|string',
      'a' => 'required|string',
      'b' => 'required|string',
      'c' => 'required|string',
      'd' => 'required|string',
      'answer' => 'required|string',
    ]);
    $question = ExamQuestion::find($request->question_id);
    $question->question = $request->question;
    $question->a = $request->a;
    $question->b = $request->b;
    $question->c = $request->c;
    $question->d = $request->d;
    $question->answer = $request->answer;
    $question->save();
    return back();
  }

  public function deleteQuestion(Request $request) {
    $question = ExamQuestion::find($request->question_id_del);
    $question->delete();
    return back();
  }

  public function examResults($sched_id, $exam_id) {
    $this->checkUser();
    $teacher = $this->getTeacher();
    $scheds = $teacher->teachersched;
    $sched = $this->getSchedule($sched_id, $scheds);
    $strand = Strand::find($sched->strand_id);
    $exam = Exam::find($exam_id);
    $examHeads = $exam->answers->where('submitted', '1');
    return view('teacher.exam-results')
    ->with('scheds', $scheds)
    ->with('sched', $sched)
    ->with('teacher', $teacher)
    ->with('strand', $strand)
    ->with('exam', $exam)
    ->with('examHeads', $examHeads);
  }
}
