<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\Activity;
use App\Strand;
use App\Schedule;
use App\SubmittedActivity;
use App\Calendar as Event;
use App\Exam;
use App\AnsweredExam;
use App\TemporaryAnswer;
use App\ExamAnswers;
use App\GradingHead;
use App\Settings;
use Auth;

class StudentController extends Controller
{
  private function checkUser() {
    if(auth()->user()->usertype != "Student") {
      abort(403, 'Unauthorized action.');
    }
  }

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $setting = Settings::all()->first();
    return view('student.index')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('setting', $setting);
  }

  public function myclass() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $setting = Settings::all()->first();
    return view('student.myclass')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('setting', $setting);
  }

  public function Activity($sched_id) {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $activities = Activity::where('sched_id', $sched_id)->orderBy('created_at', 'des')->get();
    $sched = Schedule::find($sched_id);
    return view('student.activity')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('activities', $activities)
    ->with('sched', $sched);
  }

  public function showActivity($sched_id, $act_id) {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $activity = Activity::find($act_id);
    $sched = Schedule::find($sched_id);
    return view('student.showActivity')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('activity', $activity)
    ->with('sched', $sched);
  }

  public function answerActivity(Request $request) {
    $students = Student::where('user_id', auth()->user()->id)->get();
    $student = $user->student->first();
    $this->validate($request, [
      'answer' => 'string',
      'file' => 'mimes:doc,docx|nullable|max:4999',
    ]);

    //file upload
    if($request->hasFile('file')) {
      //get filename w/ ext
      $filenameWithExt = $request->file('file')->getClientOriginalName();
      //get just file name
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
      //get just ext
      $ext = $request->file('file')->getClientOriginalExtension();
      //file name to store
      $fileNameToStore = $filename . "_" . time() . "." . $ext;
      //upload image
      $path = $request->file('file')->storeAs('public/submitted-assignments', $fileNameToStore);
    } else {
      $fileNameToStore = '';
    }

    $answer = new SubmittedActivity;
    $answer->activity_id = $request->act_id;
    $answer->student_id = $student->student_id;
    $answer->schedule_id = $request->sched_id;
    $answer->answer = $request->answer;
    $answer->file = $fileNameToStore;
    $answer->save();
    return back();
  }

  public function grades() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    //return $student->grades;
    return view('student.grades')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('i', 0);
  }

  public function calendar() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $data = array();
    $strand = Strand::find($student->strand);
    $events = Event::where('strand_id', $strand)->get();
    foreach($events as $row)
    {
      $data[] = array(
        'id'   => $row["id"],
        'title'   => $row["title"],
        'start'   => $row["start_date"],
        'end'   => $row["end_date"],
        'description'   => $row["description"],
        'strand'   => $row["strand_id"],
      );
    }
    return view('student.calendar')->with('events', json_encode($data));
  }

  public function exams($sched_id) {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $sched = Schedule::find($sched_id);
    return view('student.exams')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('exams', $sched->exams)
    ->with('sched', $sched);
  }

  public function viewExam($sched_id, $exam_id) {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $exam = Exam::find($exam_id);
    $sched = Schedule::find($sched_id);
    foreach ($student->answeredExams->where('exam_id', $exam_id) as $s) {
      $examHead = $s;
    }
    if(isset($examHead) > 0) {
      return view('student.showExam')
      ->with('student', $student)
      ->with('strand', $strand)
      ->with('exam', $exam)
      ->with('sched', $sched)
      ->with('examHead', $examHead);
    } else {
      return view('student.showExam')
      ->with('student', $student)
      ->with('strand', $strand)
      ->with('exam', $exam)
      ->with('sched', $sched);
    }

  }

  public function answerExam($sched_id, $exam_id) {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $exam = Exam::find($exam_id);
    $sched = Schedule::find($sched_id);
    $answered = AnsweredExam::where('exam_id', $exam_id)->where('student_id', $student->student_id)->get();
    foreach ($answered as $a) {
      if($a->submitted == true) {
        return redirect("/student/exams/" . $sched->id . "/" . $exam->id)->with('alreadyAnswered', 'Error: You already answered this exam.');
      }
    }
    if(count($answered) == 0) {
      $answered = new AnsweredExam;
      $answered->exam_id = $exam_id;
      $answered->student_id = $student->student_id;
      $answered->save();
    }
    return view('student.answerExam')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('exam', $exam)
    ->with('sched', $sched)
    ->with('num', 0);
  }

  public function storeTempAnswer(Request $request) {
    $temp = TemporaryAnswer::where('student_id', $request->student)->where('question_id', $request->question)->get();
     if(count($temp) == 0) {
      $temp = new TemporaryAnswer;
      $temp->student_id = $request->student;
      $temp->question_id = $request->question;
      $temp->answer = $request->answer;
      $temp->save();
     } else {
       foreach ($temp as $t) {
         $t->answer = $request->answer;
         $t->save();
       }
     }
    return back();
  }

  public function submitExamAnswer($sched_id, $exam_id) {
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $exam = Exam::find($exam_id);
    foreach ($student->answeredExams->where('exam_id', $exam_id) as $s) {
      $examHead = $s;
    }
    $sched = Schedule::find($sched_id);
    $strand = Strand::find($student->strand);
    $score = 0;
    $overall = count($exam->questions);
    foreach ($exam->questions as $q) {
      $answer = new ExamAnswers;
      $answer->exam_answers_id = $examHead->id;
      $answer->question_id = $q->id;
      foreach ($q->tempQuestions->where('student_id', $student->student_id) as $a) {
        $answer->answer = $a->answer;
      }
      $answer->correct_answer = $q->answer;
      if ($answer->answer == $q->answer) {
        $answer->remarks = "Correct";
        $score++;
      } else {
        $answer->remarks = "Wrong";
      }
      $answer->save();
    }
    $examHead = AnsweredExam::find($examHead->id);
    $examHead->submitted = 1;
    $examHead->score = $score;
    if($score > ($overall/2)) {
      $examHead->remarks = "Passed";
    } else {
      $examHead->remarks = "Failed";
    }
    $examHead->save();
    return redirect("/student/exams/" . $sched->id . "/" . $exam->id)->with('ExamDone', 'You successfully answered this exam.');
  }

  public function enroll() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $strand = Strand::find($student->strand);
    $setting = Settings::all()->first();
    return view('student.enroll')
    ->with('student', $student)
    ->with('strand', $strand)
    ->with('setting', $setting);
  }

  public function enrollStudent() {
    $this->checkUser();
    $user = User::find(auth()->user()->id);
    $student = $user->student->first();
    $setting = Settings::all()->first();
    $student->enrolled = 1;
    $student->save();
    $gradingHead = new GradingHead;
    $gradingHead->grade = $student->grade;
    $gradingHead->semester = $setting->semester;
    $gradingHead->student_id = $student->student_id;
    $gradingHead->strand_id = $student->strand;
    $gradingHead->school_year = $setting->school_year;
    $gradingHead->save();
    return redirect('/student/enroll')->with('success', 'Enrolled Successfully');
  }
}
