<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar as Event;

class CalendarController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $events = Event::all();
    $data = array();
    foreach($events as $row)
    {
      $data[] = array(
        'id'   => $row["id"],
        'title'   => $row["title"],
        'start'   => $row["start_date"],
        'end'   => $row["end_date"],
        'description'   => $row["description"]
      );
    }
    return view('calendar')->with('events', json_encode($data));
    //return json_encode($data);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $this->validate($request, [
      'title' => 'required|string|max:255',
      'start_date' => 'required|date',
      'end_date' => 'required|date',
      'description' => 'nullable|string',
    ]);
    $event = new Event;
    $event->title = $request->title;
    $event->start_date = $request->start_date;
    $event->end_date = $request->end_date;
    $event->description = $request->description;
    $event->save();
    return redirect('/Admin/calendar')->with('success', 'Event Added');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request)
  {
    $this->validate($request, [
      'title' => 'required|string|max:255',
      'start_date' => 'required|date',
      'end_date' => 'required|date',
      'description' => 'nullable|string',
    ]);
    $event = Event::find($request->event_id);
    $event->title = $request->title;
    $event->start_date = $request->start_date;
    $event->end_date = $request->end_date;
    $event->description = $request->description;
    $event->save();
    return redirect('/Admin/calendar');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request)
  {
    $event = Event::find($request->delete_id);
    $event->delete();
    return redirect('/Admin/calendar')->with('success', 'Event Deleted');
  }
}
