<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmittedActivity extends Model
{
    protected $table = 'submittedactivities';

    public function activity()
    {
      return $this->belongsTo('App\Activity', 'activity_id');
    }

    public function student()
    {
      return $this->belongsTo('App\Student', 'student_id');
    }

}
