<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
  public function teacher()
  {
    return $this->belongsTo('App\TeacherProfile', 'teacher_id');
  }

  public function schedule()
  {
    return $this->belongsTo('App\Schedule', 'sched_id');
  }

  public function answers()
  {
    return $this->hasMany('App\SubmittedActivity', 'activity_id');
  }
}
