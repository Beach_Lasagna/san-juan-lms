<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
  protected $table = 'exams';

  public function questions() {
    return $this->hasMany('App\ExamQuestion', 'exam_id');
  }

  public function schedule() {
    return $this->belongsTo('App\Schedule', 'sched_id');
  }

  public function answers() {
    return $this->hasMany('App\AnsweredExam', 'exam_id');
  }
}
