<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporaryAnswer extends Model
{
    protected $table = 'temp_answers';

    public function question() {
      return $this->belongsTo('App\ExamQuestion', 'question_id');
    }
}
