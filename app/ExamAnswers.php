<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamAnswers extends Model
{
    protected $table = 'exam_answers_details';

    public function head() {
      return $this->belongsTo('App\AnsweredExam', 'exam_answers_id');
    }
}
